use crate::{SharedServiceState, SharedCatcherContext, epp::Epp};

use std::time::Duration;

use chrono::{DateTime, Utc, NaiveDate, NaiveDateTime};

use common::logger::Logger;
use common::domain::{DomainCatchRequest, CatchSettingStatus, CatcherStatus};

use tokio::time;
use tokio::task;
use tokio::net::TcpStream;
use tokio::io::{BufReader, AsyncWriteExt, AsyncBufReadExt};
use statrs::statistics::Statistics;

#[derive(Debug, Clone)]
pub struct CatcherContext {
    pub status: CatchSettingStatus,
    pub domain_catch_request: DomainCatchRequest,
    pub abort: bool,
}

#[derive(Debug, Clone)]
pub struct Catcher {
    pub svc: SharedServiceState,
    pub ctx: SharedCatcherContext,
}

impl Catcher {
    pub fn new(svc: SharedServiceState, ctx: SharedCatcherContext) -> Self {
        Self { svc, ctx }
    }

    pub fn _get_dts(&self) -> (DateTime<Utc>, DateTime<Utc>) {
        let ctx = self.ctx.lock().unwrap();
        let catch_date = ctx.domain_catch_request.domain.catch_date.clone();
        let start_time = ctx.domain_catch_request.catch_setting.start_time.clone();
        let end_time = ctx.domain_catch_request.catch_setting.end_time.clone();
        let start_ndt: NaiveDateTime =
            NaiveDate::from_ymd(catch_date.year.into(), catch_date.month.into(), catch_date.day.into())
            .and_hms(start_time.hour as u32, start_time.minute as u32, 0);
        let end_ndt: NaiveDateTime =
            NaiveDate::from_ymd(catch_date.year.into(), catch_date.month.into(), catch_date.day.into())
            .and_hms(end_time.hour as u32, end_time.minute as u32, 0);
        let start_dt = DateTime::<Utc>::from_utc(start_ndt, Utc);
        let end_dt = DateTime::<Utc>::from_utc(end_ndt, Utc);
        (start_dt, end_dt)
    }

    pub async fn catch(&mut self) {
        let mut catcher_name;
        let mut domain_catch_request;
        let catch_setting_id;
        {
            let mut ctx = self.ctx.lock().unwrap();
            catcher_name = ctx.domain_catch_request.catcher.name.clone();
            domain_catch_request = ctx.domain_catch_request.clone();
            catch_setting_id = ctx.domain_catch_request.catch_setting.id;
            ctx.status = CatchSettingStatus::Queued;
            self.svc.lock().unwrap().update_catch_setting_status(
                catch_setting_id,
                CatchSettingStatus::PickedUp as u8,
                format!("[{}]: PickedUp", Utc::now()),
            );
        }
        {
            let (start_dt, end_dt) = self._get_dts();
            if end_dt.signed_duration_since(start_dt).num_seconds() <= 0 {
                let mut svc = self.svc.lock().unwrap();
                svc.log_error(
                    Some(catch_setting_id),
                    format!("Catcher: {}, Domain: {}", catcher_name, domain_catch_request.domain.domain),
                    String::from("Catcher end date time happens before start date time!"),
                    format!("Catcher is set to run from '{}' to '{}' but end date time is before start date time", start_dt, end_dt)
                );
                self.ctx.lock().unwrap().status = CatchSettingStatus::EndTimePassed;
                svc.temporary_forgotten_domains.insert(domain_catch_request.domain.domain.clone(), Utc::now());
                svc.update_catch_setting_status(
                    catch_setting_id,
                    CatchSettingStatus::EndTimePassed as u8,
                    format!("[{}]: Error > End time has passed", Utc::now()),
                );
                return;
            }
        }
        self.svc.lock().unwrap().log_info(
            Some(catch_setting_id),
            format!("Catcher: {}, Domain: {}", catcher_name, domain_catch_request.domain.domain),
            String::from("Catch setting is picked up"),
            format!("Waiting for start ..."),
        );
        // >>>>>>>>>>>>>>>>> Wait For Catch Start Date Time
        let mut interval = time::interval(Duration::from_secs(60));
        loop {
            interval.tick().await;
            {
                let mut ctx = self.ctx.lock().unwrap();
                if ctx.abort {
                    let mut svc = self.svc.lock().unwrap();
                    svc.log_warn(
                        Some(catch_setting_id),
                        format!("Catcher: {}, Domain: {}", catcher_name, domain_catch_request.domain.domain),
                        String::from("Catch job has been aborted!"),
                        format!("Caught the catch job aborted while catching {}!", domain_catch_request.domain.domain),
                    );
                    svc.update_catch_setting_status(
                        catch_setting_id,
                        CatchSettingStatus::Aborted as u8,
                        format!("[{}]: Aborted", Utc::now()),
                    );
                    ctx.status = CatchSettingStatus::Aborted;
                    return;
                }
                catcher_name = ctx.domain_catch_request.catcher.name.clone();
                domain_catch_request = ctx.domain_catch_request.clone();
            }
            let (start_dt, end_dt) = self._get_dts();
            let now = Utc::now();
            let diff = start_dt.signed_duration_since(now);
            if diff.num_milliseconds() < 0 {
                let mut svc = self.svc.lock().unwrap();
                svc.log_info(
                    Some(catch_setting_id),
                    format!("Catcher: {}, Domain: {}", catcher_name, domain_catch_request.domain.domain),
                    String::from("Catcher is about to start!"),
                    format!("Catcher is supposed to run from '{}' to '{}' but now is '{}'. NO MODIFICATION IS PICKED UP FROM NOW ON!", start_dt, end_dt, now)
                );
                svc.update_catch_setting_status(
                    catch_setting_id,
                    CatchSettingStatus::Catching as u8,
                    format!("[{}]: Catching > Catch job is started!", Utc::now()),
                );
                svc.status = CatcherStatus::HealthyCatching;
                self.ctx.lock().unwrap().status = CatchSettingStatus::Catching;
                break;
            }
        }
        // >>>>>>>>>>>>>>>>> Establish EPP Connection
        let mut epp = Epp::new(self.svc.clone(), self.ctx.clone(), domain_catch_request.clone());
        // >>>>>>>>>>>>>>>>> Login into EPP system
        match epp.login().await {
            Ok(_) => {},
            Err(err) => {
                let mut svc = self.svc.lock().unwrap();
                svc.log_error(
                    Some(catch_setting_id),
                    format!("Catcher: {}, Domain: {}", catcher_name, domain_catch_request.domain.domain),
                    String::from("Error while logging into EPP system!"),
                    format!("{}", err)
                );
                self.ctx.lock().unwrap().status = CatchSettingStatus::Error;
                svc.temporary_forgotten_domains.insert(domain_catch_request.domain.domain.clone(), Utc::now());
                svc.update_catch_setting_status(
                    catch_setting_id,
                    CatchSettingStatus::Error as u8,
                    format!("[{}]: Error > EPP login has failed", Utc::now()),
                );
                return;
            }
        }
        // >>>>>>>>>>>>>>>>> Establish DAC connection
        let dac_server_addr = format!("{}:{}", domain_catch_request.catch_setting.dac_host, domain_catch_request.catch_setting.dac_port);
        let dac_tcp_stream = TcpStream::connect(dac_server_addr).await.unwrap();
        let (dac_tcp_reader, mut dac_tcp_writer) = dac_tcp_stream.into_split();
        let mut dac_reader = BufReader::new(dac_tcp_reader);
        // >>>>>>>>>>>>>>>>> Read limits and the current usage
        dac_tcp_writer.write_all(b"#limits\r\n").await.unwrap();
        dac_tcp_writer.write_all(b"#usage\r\n").await.unwrap();
        // >>>>>>>>>>>>>>>>> Catch Terminator Logic
        {
            let ctx_clone = self.ctx.clone();
            let svc_clone = self.svc.clone();
            let mut dac_tcp_writer_clone = dac_tcp_writer.clone();
            let catcher_name_clone = catcher_name.clone();
            let now = Utc::now();
            let (_, end_dt) = self._get_dts();
            let diff_to_end = end_dt.signed_duration_since(now);
            task::spawn_local(async move {
                let mut interval = time::interval(Duration::from_secs((diff_to_end.num_seconds() as u64) + 1));
                interval.tick().await;
                interval.tick().await;
                interval = time::interval(Duration::from_secs(1));
                loop {
                    interval.tick().await;
                    let now = Utc::now();
                    if end_dt.signed_duration_since(now).num_seconds() <= 0 {
                        dac_tcp_writer_clone.shutdown_stream();
                        let mut ctx = ctx_clone.lock().unwrap();
                        match ctx.status {
                            CatchSettingStatus::Unknown |  CatchSettingStatus::Catching => {
                                let mut svc = svc_clone.lock().unwrap();
                                ctx.status = CatchSettingStatus::DomainNeverDropped;
                                svc.log_info(
                                    Some(catch_setting_id),
                                    format!("Catcher: {}", catcher_name_clone),
                                    String::from("End time reached!"),
                                    format!("The catcher's end date time is '{}' and now is '{}'. Domain never dropped!", end_dt, now));
                                svc.update_catch_setting_status(
                                    catch_setting_id,
                                    CatchSettingStatus::DomainNeverDropped as u8,
                                    format!("[{}]: Error > Domain never dropped", Utc::now()),
                                );
                            },
                            _ => {}
                        }
                        return;
                    }
                }
            });
        }
        // >>>>>>>>>>>>>>>>> Domain Checker Logic
        {
            let svc_clone = self.svc.clone();
            let domain = domain_catch_request.domain.domain.clone();
            let dac_interval = domain_catch_request.catch_setting.dac_interval;
            let catcher_name_clone = catcher_name.clone();
            let mut dac_tcp_writer_clone = dac_tcp_writer.clone();
            task::spawn_local(async move {
                let mut interval = time::interval(Duration::from_millis((dac_interval + 1).into()));
                let mut counter: u64 = 0;
                loop {
                    interval.tick().await;
                    match dac_tcp_writer_clone.write_all(format!("{}\r\n", domain).as_bytes()).await {
                        Ok(_) => {},
                        Err(err) => {
                            svc_clone.lock().unwrap().log_error(
                                Some(catch_setting_id),
                                format!("Catcher: {}", catcher_name_clone),
                                String::from("Error while checking the domain status"),
                                format!("{}", err));
                            return;
                        }
                    }
                    counter += 1;
                    if counter % 10000 == 0 {
                        match dac_tcp_writer_clone.write_all(b"#usage\r\n").await {
                            Ok(_) => {},
                            Err(err) => {
                                svc_clone.lock().unwrap().log_error(
                                    Some(catch_setting_id),
                                    format!("Catcher: {}", catcher_name_clone),
                                    String::from("Error while checking the domain usage"),
                                    format!("{}", err));
                                return;
                            }
                        }
                    }
                }
            });
        }
        // >>>>>>>>>>>>>>>>> Process DAC Replies
        let limits_prefix = "#limits";
        let usage_prefix = "#usage";
        let free_domain_prefix = format!("{},N", domain_catch_request.domain.domain);
        let busy_domain_prefix = format!("{},Y", domain_catch_request.domain.domain);
        let dac_blocked_prefix = format!("{},B", domain_catch_request.domain.domain);
        let mut counter : u64 = 1;
        let mut marks = Vec::new();
        let mut mark = Utc::now();
        loop {
            let mut line = String::new();
            match dac_reader.read_line(&mut line).await {
                Ok(_) => {
                    marks.push(f64::from(Utc::now().signed_duration_since(mark).num_milliseconds() as i32));
                    mark = Utc::now();
                    if counter % 500 == 0 {
                        {
                            let min = marks.clone().min();
                            let max = marks.clone().max();
                            let mean = marks.clone().mean();
                            let std_dev = marks.clone().population_std_dev();
                            println!("Stats: mean: {:.2} ms, std-dev: {:.2} ms, min: {:.2} ms, max: {:.2} ms", mean, std_dev, min, max);
                            marks.clear();
                            let mut ctx = self.ctx.lock().unwrap();
                            if ctx.abort {
                                let mut svc = self.svc.lock().unwrap();
                                svc.log_warn(
                                    Some(catch_setting_id),
                                    format!("Catcher: {}, Domain: {}", catcher_name, domain_catch_request.domain.domain),
                                    String::from("Catch job has been aborted!"),
                                    format!("Caught the catch job aborted while catching {}!", domain_catch_request.domain.domain),
                                );
                                dac_tcp_writer.shutdown_stream();
                                epp.close().await.ok();
                                ctx.status = CatchSettingStatus::Aborted;
                                svc.update_catch_setting_status(
                                    catch_setting_id,
                                    CatchSettingStatus::Aborted as u8,
                                    format!("[{}]: Aborted", Utc::now()),
                                );
                                return;
                            }
                        }
                        let mut line_clone = line.clone();
                        let svc_clone = self.svc.clone();
                        let ctx_clone = self.ctx.clone();
                        let catcher_name_clone = catcher_name.clone();
                        let domain_clone = domain_catch_request.domain.domain.clone();
                        let mut dac_tcp_writer_clone = dac_tcp_writer.clone();
                        dac_tcp_writer_clone.shutdown_on_drop = false;
                        task::spawn_local(async move {
                            let v: Vec<&str> = line_clone.split(',').collect();
                            if v.len() < 5 {
                                return;
                            }
                            if v[1] == "Y" {
                                match NaiveDateTime::parse_from_str(format!("{} 00:00:00", v[4]).as_str(), "%Y-%m-%d %H:%M:%S") {
                                    Ok(to_ndt) => {
                                        let to_dt = DateTime::<Utc>::from_utc(to_ndt, Utc);
                                        let diff = to_dt.signed_duration_since(Utc::now());
                                        if diff.num_seconds() > 0 {
                                            line_clone.pop();
                                            line_clone.pop();
                                            let mut svc = svc_clone.lock().unwrap();
                                            ctx_clone.lock().unwrap().status = CatchSettingStatus::DomainLost;
                                            svc.lost_forever_domains.push(domain_clone.clone());
                                            dac_tcp_writer_clone.shutdown_stream();
                                            svc.log_error(
                                                Some(catch_setting_id),
                                                format!("Catcher: {}, Domain: {}", catcher_name_clone, domain_clone),
                                                String::from("Domain has already been registered!"),
                                                format!("Domain has already been registered until '{}'. (The DAC line was [{}])", to_dt, line_clone));
                                            svc.update_catch_setting_status(
                                                catch_setting_id,
                                                CatchSettingStatus::DomainLost as u8,
                                                format!("[{}]: Domain Lost [{}]", Utc::now(), line_clone),
                                            );
                                        }
                                    },
                                    Err(err) => {
                                        svc_clone.lock().unwrap().log_error(
                                            Some(catch_setting_id),
                                            format!("Catcher: {}, Domain: {}", catcher_name_clone, domain_clone),
                                            String::from("Error while parsing the 'to' datetime!"),
                                            format!("{}", err));
                                    }
                                }
                            }
                        });
                    }
                    counter += 1;
                    if line.is_empty() {
                        let mut svc = self.svc.lock().unwrap();
                        svc.log_info(
                            Some(catch_setting_id),
                            format!("Catcher: {}, Domain: {}", catcher_name, domain_catch_request.domain.domain),
                            String::from("Error while reading the domain status"),
                            format!("DAC connection is closed!"));
                        epp.close().await.ok();
                        return;
                    }
                    line = line.trim().to_string();
                    // Trim '\r\n' from end of the read line
                    // line.pop(); line.pop();
                    /*{
                        self.svc.lock().unwrap().log_info(
                            format!("Catcher: {}, Domain: {}", catcher_name, domain_catch_request.domain.domain),
                            String::from("DAC check result"),
                            format!("{}", line));
                    }*/
                    if line.starts_with(limits_prefix) {
                        {
                            self.svc.lock().unwrap().log_info(
                                Some(catch_setting_id),
                                format!("Catcher: {}, Domain: {}", catcher_name, domain_catch_request.domain.domain),
                                String::from("DAC limits"),
                                format!("{}", line));
                        }
                    } else if line.starts_with(usage_prefix) {
                        self.svc.lock().unwrap().log_info(
                            Some(catch_setting_id),
                            format!("Catcher: {}, Domain: {}", catcher_name, domain_catch_request.domain.domain),
                            String::from("DAC usage"),
                            format!("{}", line));
                    } else if line.starts_with(free_domain_prefix.as_str()) {
                        println!("Readings: {:?}", marks);
                        marks.clear();
                        mark = Utc::now();
                        match epp.register().await {
                            Ok(_) => {
                                let diff = Utc::now().signed_duration_since(mark).num_milliseconds();
                                println!("Registration took {:.2} ms", diff);
                                let mut svc = self.svc.lock().unwrap();
                                svc.log_info(
                                    Some(catch_setting_id),
                                    format!("Catcher: {}, Domain: {}", catcher_name, domain_catch_request.domain.domain),
                                    String::from("Domain registeration successful!"),
                                    String::from("Domain is registered successfully!")
                                );
                                epp.close().await.ok();
                                dac_tcp_writer.shutdown_stream();
                                self.ctx.lock().unwrap().status = CatchSettingStatus::DomainRegistered;
                                svc.lost_forever_domains.push(domain_catch_request.domain.domain);
                                svc.update_catch_setting_status(
                                    catch_setting_id,
                                    CatchSettingStatus::DomainRegistered as u8,
                                    format!("[{}]: Domain Registered", Utc::now()),
                                );
                                return;
                            },
                            Err(err) => {
                                let diff = Utc::now().signed_duration_since(mark).num_milliseconds();
                                println!("Registration failure took {:.2} ms", diff);
                                let mut svc = self.svc.lock().unwrap();
                                svc.log_error(
                                    Some(catch_setting_id),
                                    format!("Catcher: {}, Domain: {}", catcher_name, domain_catch_request.domain.domain),
                                    String::from("Domain registration failed!"),
                                    format!("{}", err)
                                );
                                /*epp.close().await.ok();
                                dac_tcp_writer.shutdown_stream();
                                self.ctx.lock().unwrap().status = CatchSettingStatus::DomainLost;
                                svc.lost_forever_domains.push(domain_catch_request.domain.domain);
                                svc.update_catch_setting_status(
                                    catch_setting_id,
                                    CatchSettingStatus::DomainLost as u8,
                                    format!("[{}]: Domain Lost", Utc::now()),
                                );
                                return;*/
                            }
                        }
                    } else if line.starts_with(busy_domain_prefix.as_str()) {
                        // TODO
                    } else if line.starts_with(dac_blocked_prefix.as_str()) {
                        match dac_tcp_writer.write_all(b"#usage\r\n").await {
                            Ok(_) => {},
                            Err(err) => {
                                let mut svc = self.svc.lock().unwrap();
                                svc.log_error(
                                    Some(catch_setting_id),
                                    format!("Catcher: {}", catcher_name),
                                    String::from("Error while checking the domain usage (while in blocked situation)"),
                                    format!("{}", err));
                                epp.close().await.ok();
                                dac_tcp_writer.shutdown_stream();
                                self.ctx.lock().unwrap().status = CatchSettingStatus::Error;
                                svc.temporary_forgotten_domains.insert(domain_catch_request.domain.domain.clone(), Utc::now());
                                svc.update_catch_setting_status(
                                    catch_setting_id,
                                    CatchSettingStatus::Error as u8,
                                    format!("[{}]: Error", Utc::now()),
                                );
                                return;
                            }
                        }
                        let mut svc = self.svc.lock().unwrap();
                        svc.log_error(
                            Some(catch_setting_id),
                            format!("Catcher: {}, Domain: {}", catcher_name, domain_catch_request.domain.domain),
                            String::from("Error while reading the domain status"),
                            format!("DAC is blocked!: {}", line));
                        let v: Vec<&str> = line.split(',').collect();
                        if v.len() > 3 && v[1] == "B" {
                            match v[2].parse::<i32>() {
                                Ok(blocked_seconds) => {
                                    if blocked_seconds > 15 * 60 {
                                        epp.close().await.ok();
                                        dac_tcp_writer.shutdown_stream();
                                        self.ctx.lock().unwrap().status = CatchSettingStatus::Blocked;
                                        svc.temporary_forgotten_domains.insert(domain_catch_request.domain.domain.clone(), Utc::now());
                                        svc.update_catch_setting_status(
                                            catch_setting_id,
                                            CatchSettingStatus::Blocked as u8,
                                            format!("[{}]: Blocked for {} secs", Utc::now(), blocked_seconds),
                                        );
                                        return;
                                    }
                                },
                                Err(err) => {
                                    self.svc.lock().unwrap().log_error(
                                        Some(catch_setting_id),
                                        format!("Catcher: {}, Domain: {}", catcher_name, domain_catch_request.domain.domain),
                                        String::from("Error while parsing block seconds to int32"),
                                        format!("We tried to convert '{}' in line '{}' to in32 and this is the error: {}", v[2], line, err));
                                },
                            }
                        } else {
                            svc.log_error(
                                Some(catch_setting_id),
                                format!("Catcher: {}, Domain: {}", catcher_name, domain_catch_request.domain.domain),
                                String::from("Wrong blocked response"),
                                format!("Block response is '{}'", line));
                        }
                    } else {
                        self.svc.lock().unwrap().log_warn(
                            Some(catch_setting_id),
                            format!("Catcher: {}, Domain: {}", catcher_name, domain_catch_request.domain.domain),
                            String::from("Unknown DAC check result"),
                            format!("'{}', Free prefix: '{}'", line, free_domain_prefix));
                    }
                },
                Err(err) => {
                    let mut svc = self.svc.lock().unwrap();
                    svc.log_error(
                        Some(catch_setting_id),
                        format!("Catcher: {}, Domain: {}", catcher_name, domain_catch_request.domain.domain),
                        String::from("Error while reading the domain status"),
                        format!("{}", err));
                    epp.close().await.ok();
                    dac_tcp_writer.shutdown_stream();
                    self.ctx.lock().unwrap().status = CatchSettingStatus::Error;
                    svc.temporary_forgotten_domains.insert(domain_catch_request.domain.domain.clone(), Utc::now());
                    svc.update_catch_setting_status(
                        catch_setting_id,
                        CatchSettingStatus::Error as u8,
                        format!("[{}]: Error", Utc::now()),
                    );
                    return;
                }
            }
        }
    }
}
