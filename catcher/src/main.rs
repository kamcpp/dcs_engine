mod catcher;
mod epp;

use std::sync::{Arc, Mutex};
use std::time::Duration;
use std::clone::Clone;
use std::ops::DerefMut;
use std::cmp::Ordering;
use std::collections::HashMap;

use futures::join;

use tokio::time;
use tokio::task;
use tokio::net::TcpStream;
use tokio::io::{BufReader, AsyncWriteExt, AsyncBufReadExt};

use tide::Request;

use chrono::{DateTime, Utc, NaiveDate, NaiveDateTime};

use common::logger::{Logger, LogEntry, LogSeverity};
use common::domain::{Date, Time, DomainCatchRequest, CatcherStatus, StatusEntry, CatchSettingStatus};

use catcher::{CatcherContext, Catcher};

use derivative::Derivative;

type Result<T> = std::result::Result<T, Box<dyn std::error::Error + Send + Sync + 'static>>;

extern crate serde_derive;

type SharedServiceState = Arc<Mutex<ServiceState>>;
type SharedCatcherContext = Arc<Mutex<CatcherContext>>;

#[derive(Derivative, Debug, Clone)]
#[derivative(Hash, PartialEq, Eq)]
struct DomainCatchRequestKey {
    catch_date: Date,
    start_time: Time,
    domain: String,
    priority: u8,
    #[derivative(PartialEq="ignore")]
    #[derivative(Hash="ignore")]
    catcher_context: SharedCatcherContext,
}

fn get_dt(date: &Date, time: &Time) -> DateTime<Utc> {
    let ndt: NaiveDateTime =
        NaiveDate::from_ymd(date.year.into(), date.month.into(), date.day.into())
        .and_hms(time.hour as u32, time.minute as u32, 0);
    DateTime::<Utc>::from_utc(ndt, Utc)
}

impl Ord for DomainCatchRequestKey {
    fn cmp(&self, other: &Self) -> Ordering {
        let result = self.catch_date.cmp(&other.catch_date);
        if result != Ordering::Equal {
            return if result == Ordering::Greater { Ordering::Less } else { Ordering::Greater };
        }
        let now = Utc::now();
        let self_start_dt = get_dt(&self.catch_date, &self.start_time);
        let other_start_dt = get_dt(&other.catch_date, &other.start_time);
        if self_start_dt.signed_duration_since(now).num_seconds() > 0 ||
           other_start_dt.signed_duration_since(now).num_seconds() > 0 {
            let result = self.start_time.cmp(&other.start_time);
            if result != Ordering::Equal {
                return if result == Ordering::Greater { Ordering::Less } else { Ordering::Greater };
            }
        }
        let result = self.priority.cmp(&other.priority);
        if result != Ordering::Equal {
            return if result == Ordering::Greater { Ordering::Less } else { Ordering::Greater };
        }
        let result = self.domain.cmp(&other.domain);
        if result != Ordering::Equal {
            return if result == Ordering::Greater { Ordering::Less } else { Ordering::Greater };
        }
        return Ordering::Equal;
    }
}

impl PartialOrd for DomainCatchRequestKey {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

#[derive(Debug)]
pub struct ServiceState {
    status: CatcherStatus,
    domain_catch_requests_to_process: Vec<DomainCatchRequest>,
    candidate_key: Option<DomainCatchRequestKey>,
    running_catcher_key: Option<DomainCatchRequestKey>,
    log_entries: Vec<LogEntry>,
    status_entries: Vec<StatusEntry>,
    lost_forever_domains: Vec<String>,
    temporary_forgotten_domains: HashMap<String, DateTime<Utc>>,
}

impl ServiceState {
    fn new() -> Self {
        ServiceState {
            status: CatcherStatus::HealthyFree,
            domain_catch_requests_to_process: Vec::new(),
            candidate_key: None,
            running_catcher_key: None,
            log_entries: Vec::new(),
            status_entries: Vec::new(),
            lost_forever_domains: Vec::new(),
            temporary_forgotten_domains: HashMap::new(),
        }
    }

    pub fn update_catcher_status(&mut self, id: u64, code: u8, message: String) {
        self._status("catchers".to_string(), id, code, message);
    }

    pub fn update_catch_setting_status(&mut self, id: u64, code: u8, message: String) {
        self._status("catchsettings".to_string(), id, code, message);
    }

    fn _status(&mut self, entity: String, id: u64, code: u8, message: String) {
        let status_entry = StatusEntry { entity, id, code, message };
        println!("S> {:?}", status_entry);
        self.status_entries.push(status_entry);
    }

    fn _log(&mut self, catch_setting_id: Option<u64>, severity: i8, source: String, title: String, msg: String) {
        let log_entry = LogEntry { catch_setting_id, time: Utc::now().timestamp_millis(), severity, source, title, msg };
        println!("L> {:?}", log_entry);
        self.log_entries.push(log_entry)
    }
}

impl Logger for ServiceState {
    fn log_trace(&mut self, catch_setting_id: Option<u64>, source: String, title: String, msg: String) {
        self._log(catch_setting_id, LogSeverity::Trace as i8, source, title, msg)
    }

    fn log_debug(&mut self, catch_setting_id: Option<u64>, source: String, title: String, msg: String) {
        self._log(catch_setting_id, LogSeverity::Debug as i8, source, title, msg)
    }

    fn log_info(&mut self, catch_setting_id: Option<u64>,  source: String, title: String, msg: String) {
        self._log(catch_setting_id, LogSeverity::Info as i8, source, title, msg)
    }

    fn log_warn(&mut self, catch_setting_id: Option<u64>,  source: String, title: String, msg: String) {
        self._log(catch_setting_id, LogSeverity::Warning as i8, source, title, msg)
    }

    fn log_error(&mut self, catch_setting_id: Option<u64>,  source: String, title: String, msg: String) {
        self._log(catch_setting_id, LogSeverity::Error as i8, source, title, msg)
    }

    fn log_fatal(&mut self, catch_setting_id: Option<u64>,  source: String, title: String, msg: String) {
        self._log(catch_setting_id, LogSeverity::Fatal as i8, source, title, msg)
    }
}

async fn handle_submit_req(mut req: Request<SharedServiceState>) -> tide::Result<String> {
    let domain_catch_requests: Vec<DomainCatchRequest>;
    match req.body_json().await {
        Ok(parsed_json) => domain_catch_requests = parsed_json,
        Err(err) => {
            println!("ERROR while parsing domain catch request: {}", err);
            return Err(err);
        }
    }
    let mut state = req.state().lock().unwrap();
    state.domain_catch_requests_to_process = domain_catch_requests;
    Ok(String::from("ok"))
}

async fn handle_ping(req: Request<SharedServiceState>) -> tide::Result<String> {
    let state = req.state().lock().unwrap();
    Ok(format!("pong|{}|{}", state.status.clone() as u8, Utc::now().to_string()))
}

async fn handle_logs(req: Request<SharedServiceState>) -> tide::Result<String> {
    let mut state = req.state().lock().unwrap();
    let logs = serde_json::to_string(&state.log_entries)?;
    state.log_entries.clear();
    Ok(logs)
}

async fn handle_statuses(req: Request<SharedServiceState>) -> tide::Result<String> {
    let mut state = req.state().lock().unwrap();
    let statuses = serde_json::to_string(&state.status_entries)?;
    state.status_entries.clear();
    Ok(statuses)
}

async fn handle_dac(mut req: Request<SharedServiceState>) -> tide::Result<String> {
    let domain = req.body_string().await.unwrap();
    let mut rt = tokio::runtime::Runtime::new()?;
    let local = task::LocalSet::new();
    let result = Arc::new(Mutex::new(String::new()));
    {
        let result = result.clone();
        local.block_on(&mut rt, async move {
            let dac_server_addr = "dac.nic.uk:2043".to_string();
            let dac_tcp_stream = TcpStream::connect(dac_server_addr).await.unwrap();
            let mut dac_reader = BufReader::new(dac_tcp_stream);
            dac_reader.write_all(format!("{}\r\n", domain).as_bytes()).await.unwrap();

            let mut line = String::new();
            match dac_reader.read_line(&mut line).await {
                Ok(_) => {
                    let mut res = result.lock().unwrap();
                    *res = line;
                },
                Err(err) => {
                    println!("Error at /api/catcher/dac: {}", err);
                    let mut res = result.lock().unwrap();
                    *res = "error".to_string();
                }
            }
        });
    }
    let to_return = result.lock().unwrap();
    Ok(to_return.clone())
}

fn main() -> std::result::Result<(), std::io::Error> {
    let service_state = Arc::new(Mutex::new(ServiceState::new()));
    let mut rt = tokio::runtime::Runtime::new()?;
    let local = task::LocalSet::new();
    local.block_on(&mut rt, async move {
        println!("Hello! Catcher is running now ...");
        service_state.lock().unwrap().log_info(
            None,
            "Catcher".to_string(),
            "Hello!".to_string(),
            "Catcher is running now ...".to_string(),
        );
        let request_processor_job_clone = service_state.clone();
        let request_processor_job = task::spawn_local(async move {
            let mut interval = time::interval(Duration::from_secs(10));
            loop {
                interval.tick().await;

                let mut guard = request_processor_job_clone.lock().unwrap();
                let mut service_state = guard.deref_mut();

                let mut log_entries = Vec::new();
                let mut status_entries = Vec::new();

                let now = Utc::now();

                service_state.temporary_forgotten_domains.retain(|domain, &mut insertion_dt| {
                    let should_retain = Utc::now().signed_duration_since(insertion_dt).num_seconds() < 60;
                    if !should_retain {
                        println!("'{}' has been removed from temporary forgotten domains", domain);
                    }
                    return should_retain;
                });

                match &service_state.running_catcher_key {
                    Some(running_catcher_key) => {
                        let mut running_domain_found_in_requests = false;
                        let mut abort_running_catcher_job_becuase_of_a_higher_priority_request = false;
                        for domain_catch_request in &service_state.domain_catch_requests_to_process {

                            if service_state.lost_forever_domains.contains(&domain_catch_request.domain.domain) {
                                continue;
                            }

                            if service_state.temporary_forgotten_domains.contains_key(&domain_catch_request.domain.domain) {
                                continue;
                            }

                            let catch_date = domain_catch_request.domain.catch_date.clone();
                            // let start_dt = get_dt(&catch_date, &domain_catch_request.catch_setting.start_time);
                            let end_dt = get_dt(&catch_date, &domain_catch_request.catch_setting.end_time);

                            if domain_catch_request.domain.domain == running_catcher_key.domain {
                                running_domain_found_in_requests = true;
                            }
                            if domain_catch_request.domain.domain != running_catcher_key.domain &&
                               domain_catch_request.domain.catch_date <= running_catcher_key.catch_date &&
                               domain_catch_request.domain.priority < running_catcher_key.priority &&
                               //(domain_catch_request.domain.priority < running_catcher_key.priority ||
                               // (domain_catch_request.domain.priority == running_catcher_key.priority &&
                               //  domain_catch_request.domain.domain.len() < running_catcher_key.domain.len())) &&
                               // start_dt.signed_duration_since(now).num_seconds() <= 0 &&
                               end_dt.signed_duration_since(now).num_seconds() >= 0 {
                                abort_running_catcher_job_becuase_of_a_higher_priority_request  = true;
                            }
                        }
                        if abort_running_catcher_job_becuase_of_a_higher_priority_request ||
                           (!running_domain_found_in_requests && service_state.domain_catch_requests_to_process.len() > 0) {
                            {
                                let mut running_catcher_context = running_catcher_key.catcher_context.lock().unwrap();
                                log_entries.push(LogEntry {
                                    catch_setting_id: Some(running_catcher_context.domain_catch_request.catch_setting.id),
                                    time: 0,
                                    severity: LogSeverity::Warning as i8,
                                    source: format!("'{}' catching '{}'",
                                                    running_catcher_context.domain_catch_request.catcher.name,
                                                    running_catcher_context.domain_catch_request.domain.domain),
                                    title: String::from("Aborting current catcher job!"),
                                    msg: if abort_running_catcher_job_becuase_of_a_higher_priority_request
                                            { "A higher priority job has been detected!".to_string() } else
                                            { "The job has been removed from the list of domain catch requests!".to_string() },
                                });
                                if abort_running_catcher_job_becuase_of_a_higher_priority_request {
                                    service_state.domain_catch_requests_to_process.push(running_catcher_context.domain_catch_request.clone());
                                }
                                running_catcher_context.abort = true;
                            }
                            service_state.running_catcher_key = None;
                        }
                    },
                    None => {}
                }

                let mut forgotten_domains = Vec::new();
                let mut new_candidate: Option<DomainCatchRequestKey> = None;
                for domain_catch_request in &service_state.domain_catch_requests_to_process {

                    if service_state.lost_forever_domains.contains(&domain_catch_request.domain.domain) {
                        continue;
                    }

                    if service_state.temporary_forgotten_domains.contains_key(&domain_catch_request.domain.domain) {
                        continue;
                    }

                    match &service_state.running_catcher_key {
                        Some(running_catcher_key) => {
                            if running_catcher_key.domain == domain_catch_request.domain.domain {
                                continue;
                            }
                        },
                        None => {}
                    }

                    let catch_date = domain_catch_request.domain.catch_date.clone();

                    let start_time = domain_catch_request.catch_setting.start_time.clone();
                    // let mut start_dt = get_dt(&catch_date, &start_time);

                    let end_dt = get_dt(&catch_date, &domain_catch_request.catch_setting.end_time);

                    if end_dt.signed_duration_since(now).num_seconds() > 0 {
                        if domain_catch_request.catch_setting.status_code == CatchSettingStatus::Unknown as u8 ||
                           domain_catch_request.catch_setting.status_code == CatchSettingStatus::Aborted as u8 {
                            status_entries.push(StatusEntry {
                                id: domain_catch_request.catch_setting.id,
                                entity: "".to_string(),
                                code: CatchSettingStatus::Queued as u8,
                                message: format!("[{}]: Queued", Utc::now()),
                            });
                        };
                        let catcher_context = Arc::new(Mutex::new( CatcherContext {
                            status: CatchSettingStatus::Queued,
                            domain_catch_request: domain_catch_request.clone(),
                            abort: false,
                        }));
                        // TODO: Report status
                        let key = DomainCatchRequestKey {
                            catch_date,
                            start_time,
                            domain: domain_catch_request.domain.domain.clone(),
                            priority: domain_catch_request.domain.priority,
                            catcher_context,
                        };
                        match &new_candidate {
                            Some(candidate_key) => {
                                if candidate_key < &key {
                                    new_candidate = Some(key);
                                }
                            },
                            None => {
                                new_candidate = Some(key);
                            }
                        }
                    } else {
                        status_entries.push(StatusEntry {
                            id: domain_catch_request.catch_setting.id,
                            entity: "".to_string(),
                            code: CatchSettingStatus::EndTimePassed as u8,
                            message: format!("[{}]: End time passed!", Utc::now()),
                        });
                        forgotten_domains.push(domain_catch_request.domain.domain.clone());
                    }
                }
                for forgotten_domain in forgotten_domains {
                    service_state.temporary_forgotten_domains.insert(forgotten_domain, Utc::now());
                }
                for status_entry in status_entries {
                    service_state.update_catch_setting_status(
                        status_entry.id,
                        status_entry.code,
                        status_entry.message
                    );
                }
                for log_entry in log_entries {
                    if log_entry.severity == LogSeverity::Trace as i8 {
                        service_state.log_trace(log_entry.catch_setting_id, log_entry.source, log_entry.title, log_entry.msg);
                    } else if log_entry.severity == LogSeverity::Debug as i8 {
                        service_state.log_debug(log_entry.catch_setting_id, log_entry.source, log_entry.title, log_entry.msg);
                    } else if log_entry.severity == LogSeverity::Info as i8 {
                        service_state.log_info(log_entry.catch_setting_id, log_entry.source, log_entry.title, log_entry.msg);
                    } else if log_entry.severity == LogSeverity::Warning as i8 {
                        service_state.log_warn(log_entry.catch_setting_id, log_entry.source, log_entry.title, log_entry.msg);
                    } else if log_entry.severity == LogSeverity::Error as i8 {
                        service_state.log_error(log_entry.catch_setting_id, log_entry.source, log_entry.title, log_entry.msg);
                    }
                }
                service_state.domain_catch_requests_to_process.clear();
                match new_candidate {
                    Some(new_candidate) => service_state.candidate_key = Some(new_candidate.clone()),
                    None => {}
                }
            }
        });
        let runner_job_clone = Arc::clone(&service_state);
        let runner_job = task::spawn_local(async move {
            let mut interval = time::interval(Duration::from_secs(1));
            loop {
                interval.tick().await;

                let mut catcher = None;
                {
                    let mut guard = runner_job_clone.lock().unwrap();
                    let mut service_state = guard.deref_mut();

                    match &service_state.candidate_key {
                        Some(key) => {
                            println!("Picked up {:?}", key);
                            service_state.running_catcher_key = Some(key.clone());
                            catcher = Some(Catcher::new(runner_job_clone.clone(), key.catcher_context.clone()));
                        },
                        None => {}
                    }
                }
                match catcher {
                    Some(mut catcher) => {
                        println!("Waiting for catcher to finish ...");
                        catcher.catch().await;
                        println!("Catcher finished.");
                        {
                            let mut guard = runner_job_clone.lock().unwrap();
                            let mut service_state = guard.deref_mut();
                            service_state.status = CatcherStatus::HealthyFree;
                            service_state.running_catcher_key = None;
                            service_state.candidate_key = None;
                        }
                    },
                    None => {}
                }
            }
        });
        let rest_api_clone = service_state.clone();
        let rest_api_job = task::spawn_local(async move {
            let mut app = tide::with_state(rest_api_clone);
            app.at("/api/catcher/submit_req").post(handle_submit_req);
            app.at("/api/catcher/ping").post(handle_ping);
            app.at("/api/catcher/logs").post(handle_logs);
            app.at("/api/catcher/statuses").post(handle_statuses);
            app.at("/api/catcher/dac").post(handle_dac);
            app.listen("0.0.0.0:9000").await.expect("Could not start REST server at 0.0.0.0:9000");
        });
        let (r1, r2, r3) = join!(request_processor_job, runner_job, rest_api_job);
        r1.expect("Could not start request processor job!");
        r2.expect("Could not start runner job!");
        r3.expect("Could not start REST API job!");
    });
    Ok(())
}
