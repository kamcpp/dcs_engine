use crate::Result;
use crate::{SharedServiceState, SharedCatcherContext, DomainCatchRequest};

use common::logger::Logger;

use std::time::Duration;
use std::sync::{Arc, Mutex};

use native_tls::TlsConnector;

use tokio_native_tls::TlsStream;

use chrono::Utc;
use tokio::task;
use tokio::time;
use tokio::io::{BufReader, AsyncWriteExt, AsyncBufReadExt};
use tokio::net::TcpStream;

#[derive(Debug, Clone)]
pub struct Epp {
    svc: SharedServiceState,
    catcher_ctx: SharedCatcherContext,
    socket: Option<Arc<Mutex<BufReader<TlsStream<TcpStream>>>>>,
    host: String,
    port: u16,
    tag: String,
    password: String,
    domain: String,
    create_domain_msg: String,
}

macro_rules! hello_msg_fmt {() => ("
<?xml version=\"1.0\" encoding=\"UTF-8\"?>
  <epp xmlns=\"urn:ietf:params:xml:ns:epp-1.0\"
       xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
       xsi:schemaLocation=\"urn:ietf:params:xml:ns:epp-1.0
       epp-1.0.xsd\">
    <hello/>
  </epp>
")}

macro_rules! login_msg_fmt {() => ("
<?xml version=\"1.0\" encoding=\"UTF-8\"?>
  <epp xmlns=\"urn:ietf:params:xml:ns:epp-1.0\"
       xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
       xsi:schemaLocation=\"urn:ietf:params:xml:ns:epp-1.0 epp-1.0.xsd\">
    <command>
      <login>
        <clID>{}</clID>
        <pw>{}</pw>
        <options>
          <version>1.0</version>
          <lang>en</lang>
        </options>
        <svcs>
           <objURI>urn:ietf:params:xml:ns:domain-1.0</objURI>
           <objURI>urn:ietf:params:xml:ns:contact-1.0</objURI>
           <objURI>urn:ietf:params:xml:ns:host-1.0</objURI>
        </svcs>
      </login>
      <clTRID>ABC-12345</clTRID>
    </command>
  </epp>
")}

macro_rules! create_domain_msg_fmt {() => ("
<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<epp xmlns=\"urn:ietf:params:xml:ns:epp-1.0\"
   xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
   xsi:schemaLocation=\"urn:ietf:params:xml:ns:epp-1.0
   epp-1.0.xsd\">
   <command>
     <create>
       <domain:create
         xmlns:domain=\"urn:ietf:params:xml:ns:domain-1.0\"
         xsi:schemaLocation=\"urn:ietf:params:xml:ns:domain-1.0
         domain-1.0.xsd\">
         <domain:name>{}</domain:name>
         <domain:period unit=\"y\">{}</domain:period>
         <domain:registrant>{}</domain:registrant>
         <domain:authInfo>
           <domain:pw>**********</domain:pw>
         </domain:authInfo>
       </domain:create>
     </create>
     <clTRID>abcde12345</clTRID>
   </command>
</epp>
")}

impl Epp {
    pub fn new(
        svc: SharedServiceState,
        catcher_ctx: SharedCatcherContext,
        domain_catch_request: DomainCatchRequest) -> Self {
            Epp {
                svc,
                catcher_ctx,
                socket: None,
                host: domain_catch_request.epp_tag.host.clone(),
                port: domain_catch_request.epp_tag.port.clone(),
                tag: domain_catch_request.epp_tag.tag.clone(),
                password: domain_catch_request.epp_tag.password.clone(),
                domain: domain_catch_request.domain.domain.clone(),
                create_domain_msg: format!(create_domain_msg_fmt!(),
                                           domain_catch_request.domain.domain,
                                           domain_catch_request.catch_setting.register_period_years,
                                           domain_catch_request.epp_tag.account_id),
            }
    }

    pub async fn login(&mut self) -> Result<()> {
        let addr = format!("{}:{}", self.host, self.port);
        {
            let catcher_ctx = self.catcher_ctx.lock().unwrap();
            self.svc.lock().unwrap().log_info(
                Some(catcher_ctx.domain_catch_request.catch_setting.id),
                format!("Catcher: {}, Domain: {}", catcher_ctx.domain_catch_request.catcher.name,
                        catcher_ctx.domain_catch_request.domain.domain),
                String::from("Logging into EPP system"),
                format!("Connecting to '{}' ...", addr),
            );
        }
        let socket = TcpStream::connect(addr).await?;
        let tls_ctx = TlsConnector::builder().build().unwrap();
        let tls_ctx = tokio_native_tls::TlsConnector::from(tls_ctx);
        let socket = tls_ctx.connect(self.host.as_str(), socket).await?;
        let socket = Arc::new(Mutex::new(BufReader::new(socket)));
        {
            let mut s = socket.lock().unwrap();
            s.write_all(format!(login_msg_fmt!(), self.tag, self.password).as_bytes()).await.ok();
            loop {
                let mut line = String::new();
                s.read_line(&mut line).await.ok();
                line.pop(); line.pop();
                line = line.trim().to_string();
                if line.starts_with("<result code=") {
                    if !line.starts_with("<result code=\"1000\"") {
                        return Err(format!("Login failed! The result line was: '{}'", line).into());
                    }
                    break;
                }
            }
        }
        let catcher_ctx_clone = self.catcher_ctx.clone();
        let svc_clone = self.svc.clone();
        let socket_clone = socket.clone();
        task::spawn_local(async move {
            let mut interval = time::interval(Duration::from_secs(15 * 60));
            interval.tick().await;
            loop {
                interval.tick().await;
                {
                    let mut mark = Utc::now();
                    let mut socket = socket_clone.lock().unwrap();
                    let mut diff = Utc::now().signed_duration_since(mark).num_milliseconds();
                    println!("/hello: Unlock took {:.2} ms", diff);
                    mark = Utc::now();
                    match socket.write_all(hello_msg_fmt!().as_bytes()).await {
                        Ok(_) => {
                            diff = Utc::now().signed_duration_since(mark).num_milliseconds();
                            println!("/hello: Write took {:.2} ms", diff);
                            let catcher_ctx = catcher_ctx_clone.lock().unwrap();
                            svc_clone.lock().unwrap().log_info(
                                Some(catcher_ctx.domain_catch_request.catch_setting.id),
                                format!("Catcher: {}, Domain: {}", catcher_ctx.domain_catch_request.catcher.name,
                                        catcher_ctx.domain_catch_request.domain.domain),
                                String::from("Sending HELLO to EPP system"),
                                format!("Hello sent! Round-trip took {:.2} ms.", diff),
                            );
                        },
                        Err(err) => {
                            diff = Utc::now().signed_duration_since(mark).num_milliseconds();
                            println!("/hello: Error and it took {:.2} ms", diff);
                            let catcher_ctx = catcher_ctx_clone.lock().unwrap();
                            svc_clone.lock().unwrap().log_error(
                                Some(catcher_ctx.domain_catch_request.catch_setting.id),
                                format!("Catcher: {}, Domain: {}", catcher_ctx.domain_catch_request.catcher.name,
                                        catcher_ctx.domain_catch_request.domain.domain),
                                String::from("Connection failure when sending hello"),
                                format!("{}. Round-trip took {:.2}", err, diff),
                            );
                            break;
                        },
                    }
                }
            }
            let catcher_ctx = catcher_ctx_clone.lock().unwrap();
            svc_clone.lock().unwrap().log_info(
                Some(catcher_ctx.domain_catch_request.catch_setting.id),
                format!("Catcher: {}, Domain: {}", catcher_ctx.domain_catch_request.catcher.name,
                        catcher_ctx.domain_catch_request.domain.domain),
                String::from("Bye bye!"),
                format!("Exiting EPP keep-alive job!"),
            );
        });
        self.socket = Some(socket);
        Ok(())
    }

    pub async fn register(&mut self) -> Result<()> {
        match &self.socket {
            Some(socket) => {
                let mut mark = Utc::now();
                let mut s = socket.lock().unwrap();
                let mut diff = Utc::now().signed_duration_since(mark).num_milliseconds();
                println!("/register: Unlock took {:.2} ms", diff);
                mark = Utc::now();
                // println!("{}", self.create_domain_msg);
                s.write_all(self.create_domain_msg.as_bytes()).await?;
                diff = Utc::now().signed_duration_since(mark).num_milliseconds();
                println!("/register: Write took {:.2} ms", diff);
                mark = Utc::now();
                loop {
                    let mut line = String::new();
                    s.read_line(&mut line).await.ok();
                    line.pop();
                    // println!("{}", line);
                    if line.trim().starts_with("<result code=") {
                        if line.trim().starts_with("<result code=\"1000\"") {
                            diff = Utc::now().signed_duration_since(mark).num_milliseconds();
                            println!("/register: Finding result took {:.2} ms", diff);
                            return Ok(());
                        } else if line.trim().starts_with("<result code=\"2302\"") {
                            return Err("Tried to register domain but failed!".to_string().into());
                        } else if line.trim().starts_with("<result code=\"2201\"") {
                            return Err("Authorization error!".to_string().into());
                        } else if line.trim().starts_with("<result code=\"2104\"") {
                            return Err("Billing failure!".to_string().into());
                        } else {
                            return Err(format!("Registration failed. The result line was '{}'", line.trim()).into());
                        }
                    }
                }
            },
            None => {
                Err("EPP connection has not been established or you are not logged in!".to_string().into())
            },
        }
    }

    pub async fn close(&mut self) -> Result<()> {
        match &self.socket {
            Some(socket) => {
                let mut s = socket.lock().unwrap();
                s.shutdown().await.ok();
                Ok(())
            },
            None => {
                Err("EPP connection has not been established or you are not logged in!".to_string().into())
            },
        }
    }
}
