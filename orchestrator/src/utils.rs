use crate::Result;

use std::time::Duration;

use hyper::{Client, Request, StatusCode, Body};
use hyper::client::HttpConnector;

use hyper_timeout::TimeoutConnector;

use futures_util::future::TryFutureExt;
use futures_util::future::FutureExt;

pub async fn post<T>(uri: String, body_object: String, f: impl Fn(&str) -> Result<T>) -> Result<T> {
    let body_obj_str = body_object.to_string();
    let http = HttpConnector::new();
    let mut connector = TimeoutConnector::new(http);
    connector.set_connect_timeout(Some(Duration::from_secs(5)));
    let client = Client::builder().build(connector);
    let req = Request::post(uri).header("Content-Type", "application/json").body(Body::from(body_obj_str)).unwrap();
    let req_fut = client.request(req).map_err(|error| async move {
        return Err(format!("Error occured while sending the request: {:?}", error).into());
    }).map(|resp_err| async move {
        match resp_err {
            Ok(resp) => {
                if resp.status() == StatusCode::OK {
                    match hyper::body::to_bytes(resp).await {
                        Ok(bytes) => {
                            match std::str::from_utf8(bytes.as_ref()) {
                                Ok(body_str) => {
                                    return f(body_str);
                                },
                                Err(err) => return Err(format!("Error while converting body to utf8: {:?}", err).into()),
                            }
                        },
                        Err(err) => return Err(format!("Error while reading body: {:?}", err).into()),
                    }
                } else {
                    return Err(format!("Request failed with status: {}", resp.status()).into());
                }
            },
            Err(err) => return err.await,
        }
    });
    return req_fut.await.await;
}

