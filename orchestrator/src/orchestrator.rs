use crate::utils;
use crate::config::{Config, InMemConfig};

use common::logger::{LogEntry, LogSeverity, Logger};
use common::domain::{ServicesResult, StatusEntry};

use std::collections::HashSet;
use std::thread;

use tokio::task;

use derivative::Derivative;

use chrono::Utc;

use serde_json::error::Error;

pub const ORCHESTRATOR: &str = "Orchestrator";

#[derive(Derivative, Debug, Clone)]
#[derivative(PartialEq, Eq, Hash)]
pub struct CatcherEngine {
    #[derivative(PartialEq="ignore")]
    #[derivative(Hash="ignore")]
    pub id: u64,
    #[derivative(PartialEq="ignore")]
    #[derivative(Hash="ignore")]
    pub name: String,
    pub host: String,
    pub port: u16,

}

#[derive(Debug)]
pub struct Orchestrator {
    pub config: InMemConfig,
    pub healthy_catchers: HashSet<CatcherEngine>,
    pub dead_catchers: HashSet<CatcherEngine>,
}

impl Orchestrator {
    pub fn new() -> Self {
        Self {
            config: InMemConfig::new(),
            healthy_catchers: HashSet::new(),
            dead_catchers: HashSet::new(),
        }
    }

    pub fn send_log_entry(&mut self, log_entry: LogEntry) {
        // println!("{:?}", log_entry);
        let services_base_uri = self.config.services_base_uri();
        let log_entry_clone = log_entry.clone();
        task::spawn(async move {
            let uri = format!("{}/api/engine/logs/add", services_base_uri);
            match utils::post(uri.to_string(), serde_json::to_string(&log_entry_clone).unwrap(), |body_str| {
                let data_err: std::result::Result<ServicesResult, Error> = serde_json::from_str(body_str);
                match data_err {
                    Ok(result) => {
                        if !result.result {
                            return Err(format!("Error while adding log entry: {:?}", result).into());
                        }
                        return Ok(());
                    },
                    Err (err) => return Err(format!("Error while desering services result to json: {}", err).into()),
                }
            }).await {
                Ok(_) => {},
                Err(err) => {
                    println!("Error while sending the log entry: {}", err);
                },
            }
        });
    }

    pub fn send_status_entry(&mut self, status_entry: StatusEntry) {
        let services_base_uri = self.config.services_base_uri();
        let status_entry_clone = status_entry.clone();
        task::spawn(async move {
            let uri = format!("{}/api/engine/{}/status", services_base_uri, status_entry_clone.entity);
            match utils::post(uri.to_string(), serde_json::to_string(&status_entry_clone).unwrap(), |body_str| {
                let data_err: std::result::Result<ServicesResult, Error> = serde_json::from_str(body_str);
                match data_err {
                    Ok(result) => {
                        if !result.result {
                            return Err(format!("Error while adding status entry: {:?}", result).into());
                        }
                        return Ok(());
                    },
                    Err (err) => return Err(format!("Error while desering services result to json: {}", err).into()),
                }
            }).await {
                Ok(_) => {},
                Err(err) => {
                    println!("Error while sending the status entry: {}", err);
                },
            }
        });
    }

    fn _log(&mut self, severity: i8, source: String, title: String, msg: String) {
        self.send_log_entry(
            LogEntry {
                catch_setting_id: None,
                severity,
                time: Utc::now().timestamp_millis(),
                source: format!("{} (Thread: {})", source, thread::current().name().unwrap()),
                title,
                msg
            }
        )
    }
}

impl Logger for Orchestrator {
    fn log_trace(&mut self, _catch_setting_id: Option<u64>, source: String, title: String, msg: String) {
        self._log(LogSeverity::Trace as i8, source, title, msg)
    }

    fn log_debug(&mut self, _catch_setting_id: Option<u64>, source: String, title: String, msg: String) {
        self._log(LogSeverity::Debug as i8, source, title, msg)
    }

    fn log_info(&mut self, _catch_setting_id: Option<u64>, source: String, title: String, msg: String) {
        self._log(LogSeverity::Info as i8, source, title, msg)
    }

    fn log_warn(&mut self, _catch_setting_id: Option<u64>, source: String, title: String, msg: String) {
        self._log(LogSeverity::Warning as i8, source, title, msg)
    }

    fn log_error(&mut self, _catch_setting_id: Option<u64>, source: String, title: String, msg: String) {
        self._log(LogSeverity::Error as i8, source, title, msg)
    }

    fn log_fatal(&mut self, _catch_setting_id: Option<u64>, source: String, title: String, msg: String) {
        self._log(LogSeverity::Fatal as i8, source, title, msg)
    }
}

