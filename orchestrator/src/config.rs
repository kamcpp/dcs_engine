use std::env;

pub trait Config {
    fn services_base_uri(&self) -> String;
}

#[derive(Debug, Clone,Copy)]
pub struct InMemConfig;

impl InMemConfig {
    pub fn new() -> Self {
        Self {}
    }
}

impl Config for InMemConfig {
    fn services_base_uri(&self) -> String {
        let host = match env::var("SERVICES_HOST") {
            Ok(val) => val,
            Err(_) => "services".to_string(),
        };
        let port = match env::var("SERVICES_PORT") {
            Ok(val) => val,
            Err(_) => "8080".to_string(),
        };
        format!("http://{}:{}", host, port)
    }
}
