use crate::utils;
use crate::orchestrator::{ORCHESTRATOR, CatcherEngine, Orchestrator};

use common::logger::{LogEntry, Logger};
use common::domain::{StatusEntry, DomainCatchRequest, CatcherStatus};

use std::time::Duration;
use std::sync::{Arc, Mutex};
use std::collections::HashMap;

use tokio::task;
use tokio::time;

use chrono::Utc;

use serde_json::error::Error;

pub async fn update_catchers(orchestrator: &Arc<Mutex<Orchestrator>>, domain_catch_requests: Vec<DomainCatchRequest>) {
    let mut requests_map = HashMap::new();
    for domain_catch_request in domain_catch_requests {
        let domain_catch_request_clone = domain_catch_request.clone();
        let catcher_engine = CatcherEngine {
            id: domain_catch_request_clone.catcher.id,
            name: domain_catch_request_clone.catcher.name,
            host: domain_catch_request_clone.catcher.host,
            port: domain_catch_request_clone.catcher.port,
        };
        let requests = requests_map.entry(catcher_engine.clone()).or_insert(Vec::new());
        requests.push(domain_catch_request.clone());
        let mut inserted = false;
        {
            let mut service = orchestrator.lock().unwrap();
            if service.healthy_catchers.contains(&catcher_engine) {
                service.healthy_catchers.replace(catcher_engine.clone());
            } else if service.dead_catchers.contains(&catcher_engine) {
                service.dead_catchers.replace(catcher_engine.clone());
            } else {
                service.healthy_catchers.insert(catcher_engine.clone());
                inserted = true;
            }
        }
        if inserted {
            {
                let catcher_engine_clone = catcher_engine.clone();
                let orchestrator_clone = Arc::clone(orchestrator);
                task::spawn_local(async move {
                    let mut interval = time::interval(Duration::from_millis(5000));
                    loop {
                        interval.tick().await;
                        match utils::post(format!("http://{}:{}/api/catcher/ping", catcher_engine_clone.host, catcher_engine_clone.port), "{}".to_string(), |body_str| {
                            let mut service = orchestrator_clone.lock().unwrap();
                            let tokens: Vec<&str> = body_str.split("|").collect();
                            match (tokens.len(), tokens[0].trim(), tokens[1].trim()) {
                                (3, "pong", "1") | (3, "pong", "2") => {
                                    if service.dead_catchers.contains(&catcher_engine_clone) {
                                        service.dead_catchers.remove(&catcher_engine_clone);
                                        service.healthy_catchers.insert(catcher_engine_clone.clone());
                                        service.log_info(
                                            None,
                                            ORCHESTRATOR.to_string(),
                                            "Catcher is healthy again!".to_string(),
                                            format!("{:?}", catcher_engine_clone),
                                        );
                                    }
                                    service.send_status_entry(StatusEntry {
                                        entity: "catchers".to_string(),
                                        id: catcher_engine_clone.id,
                                        code: if tokens[1] == "1" { CatcherStatus::HealthyFree as u8 } else { CatcherStatus::HealthyCatching as u8 },
                                        message: format!("'{}': Catcher replied the ping: '{}'", Utc::now(), body_str),
                                    });
                                },
                                _ => {
                                    if service.healthy_catchers.contains(&catcher_engine_clone) {
                                        service.healthy_catchers.remove(&catcher_engine_clone);
                                        service.dead_catchers.insert(catcher_engine_clone.clone());
                                        service.log_error(
                                            None,
                                            ORCHESTRATOR.to_string(),
                                            format!("Catcher replied incorrectly while pinging it: {:?}", catcher_engine_clone),
                                            format!("Reply: '{}'", body_str),
                                        );
                                        service.send_status_entry(StatusEntry {
                                            entity: "catchers".to_string(),
                                            id: catcher_engine_clone.id,
                                            code: CatcherStatus::NotHealthy as u8 ,
                                            message: format!("'{}': Catcher replied incorrectly: '{}'", Utc::now(), body_str),
                                        });
                                    }
                                }
                            }
                            Ok(())
                        }).await {
                            Ok(_) => {},
                            Err(err) => {
                                let mut service = orchestrator_clone.lock().unwrap();
                                if service.healthy_catchers.contains(&catcher_engine_clone) {
                                    service.healthy_catchers.remove(&catcher_engine_clone);
                                    service.dead_catchers.insert(catcher_engine_clone.clone());
                                    service.log_error(
                                        None,
                                        ORCHESTRATOR.to_string(),
                                        format!("Detected a dead catcher while pinging it: {:?}", catcher_engine_clone),
                                        format!("{}", err),
                                    );
                                    service.send_status_entry(StatusEntry {
                                        entity: "catchers".to_string(),
                                        id: catcher_engine_clone.id,
                                        code: CatcherStatus::NotHealthy as u8 ,
                                        message: format!("'{}': Error happened while pinging: '{}'", Utc::now(), err),
                                    });
                                }
                            }
                        }
                    }
                });
            }
            {
                let catcher_engine_clone = catcher_engine.clone();
                let orchestrator_clone = Arc::clone(orchestrator);
                task::spawn_local(async move {
                    let mut interval = time::interval(Duration::from_millis(5000));
                    loop {
                        interval.tick().await;
                        let healthy;
                        {
                            let service = orchestrator_clone.lock().unwrap();
                            healthy = service.healthy_catchers.contains(&catcher_engine_clone);
                        }
                        if healthy {
                            // println!("Reading logs of {}:{} ...", catcher_engine_clone.host, catcher_engine_clone.port);
                            match utils::post(format!("http://{}:{}/api/catcher/logs", catcher_engine_clone.host, catcher_engine_clone.port), "{}".to_string(), |body_str| {
                                let data_err: std::result::Result<Vec<LogEntry>, Error> = serde_json::from_str(body_str);
                                match data_err {
                                    Ok(log_entries) => {
                                        for log_entry in log_entries {
                                            let mut service = orchestrator_clone.lock().unwrap();
                                            service.send_log_entry(log_entry.clone());
                                        }
                                        Ok(())
                                    },
                                    Err (err) => {
                                        let mut service = orchestrator_clone.lock().unwrap();
                                        service.log_error(
                                            None,
                                            ORCHESTRATOR.to_string(),
                                            format!("Error while deserializing logs read from the catcher: {:?}", catcher_engine_clone),
                                            format!("{}", err),
                                        );
                                        Ok(())
                                    },
                                }
                            }).await {
                                Ok(_) => {},
                                Err(err) => {
                                    let mut service = orchestrator_clone.lock().unwrap();
                                    if service.healthy_catchers.contains(&catcher_engine_clone) {
                                        service.healthy_catchers.remove(&catcher_engine_clone);
                                        service.dead_catchers.insert(catcher_engine_clone.clone());
                                        service.log_error(
                                            None,
                                            ORCHESTRATOR.to_string(),
                                            format!("Detected a dead catcher while reading the log entries: {:?}", catcher_engine_clone),
                                            format!("{}", err),
                                        );
                                        service.send_status_entry(StatusEntry {
                                            entity: "catchers".to_string(),
                                            id: catcher_engine_clone.id,
                                            code: CatcherStatus::NotHealthy as u8 ,
                                            message: format!("'{}': Error happened while reading the log entries: '{}'", Utc::now(), err),
                                        });
                                    }
                                }
                            }
                        }
                    }
                });
            }
            {
                let catcher_engine_clone = catcher_engine.clone();
                let orchestrator_clone = Arc::clone(orchestrator);
                task::spawn_local(async move {
                    let mut interval = time::interval(Duration::from_millis(5000));
                    loop {
                        interval.tick().await;
                        let healthy;
                        {
                            let service = orchestrator_clone.lock().unwrap();
                            healthy = service.healthy_catchers.contains(&catcher_engine_clone);
                        }
                        if healthy {
                            match utils::post(format!("http://{}:{}/api/catcher/statuses", catcher_engine_clone.host, catcher_engine_clone.port), "{}".to_string(), |body_str| {
                                let data_err: std::result::Result<Vec<StatusEntry>, Error> = serde_json::from_str(body_str);
                                match data_err {
                                    Ok(status_entries) => {
                                        for status_entry in status_entries {
                                            let mut service = orchestrator_clone.lock().unwrap();
                                            service.send_status_entry(status_entry.clone());
                                        }
                                        Ok(())
                                    },
                                    Err (err) => {
                                        let mut service = orchestrator_clone.lock().unwrap();
                                        service.log_error(
                                            None,
                                            ORCHESTRATOR.to_string(),
                                            format!("Error while deserializing status entries read from the catcher: {:?}", catcher_engine_clone),
                                            format!("{}", err),
                                        );
                                        Ok(())
                                    },
                                }
                            }).await {
                                Ok(_) => {},
                                Err(err) => {
                                    let mut service = orchestrator_clone.lock().unwrap();
                                    if service.healthy_catchers.contains(&catcher_engine_clone) {
                                        service.healthy_catchers.remove(&catcher_engine_clone);
                                        service.dead_catchers.insert(catcher_engine_clone.clone());
                                        service.log_error(
                                            None,
                                            ORCHESTRATOR.to_string(),
                                            format!("Detected a dead catcher while reading the status entries: {:?}", catcher_engine_clone),
                                            format!("{}", err),
                                        );
                                        service.send_status_entry(StatusEntry {
                                            entity: "catchers".to_string(),
                                            id: catcher_engine_clone.id,
                                            code: CatcherStatus::NotHealthy as u8 ,
                                            message: format!("'{}': Error happened while reading the status entries: '{}'", Utc::now(), err),
                                        });
                                    }
                                }
                            }
                        }
                    }
                });
            }
        }
    }
    for (catcher_engine, requests) in requests_map.iter() {
        let catcher_engine_clone = catcher_engine.clone();
        let orchestrator_clone = Arc::clone(orchestrator);
        let requests_clone = requests.clone();
        task::spawn_local(async move {
            let healthy;
            {
                let service = orchestrator_clone.lock().unwrap();
                healthy = service.healthy_catchers.contains(&catcher_engine_clone);
            }
            if healthy {
                match utils::post(format!("http://{}:{}/api/catcher/submit_req", catcher_engine_clone.host, catcher_engine_clone.port),
                                  serde_json::to_string(&requests_clone).unwrap(), |body_str| {
                    let mut service = orchestrator_clone.lock().unwrap();
                    if body_str != "ok" {
                        service.log_error(
                            None,
                            ORCHESTRATOR.to_string(),
                            format!("Catcher replied incorrectly while submitting domain catch requests: {:?}", catcher_engine_clone),
                            format!("Reply: '{}'", body_str),
                        );
                    }
                    Ok(())
                }).await {
                    Ok(_) => {},
                    Err(err) => {
                        let mut service = orchestrator_clone.lock().unwrap();
                        if service.healthy_catchers.contains(&catcher_engine_clone) {
                            service.healthy_catchers.remove(&catcher_engine_clone);
                            service.dead_catchers.insert(catcher_engine_clone.clone());
                            service.log_error(
                                None,
                                ORCHESTRATOR.to_string(),
                                format!("Detected a dead catcher while submitting the domain catch requests: {:?}", catcher_engine_clone),
                                format!("{}", err),
                            );
                            service.send_status_entry(StatusEntry {
                                entity: "catchers".to_string(),
                                id: catcher_engine_clone.id,
                                code: CatcherStatus::NotHealthy as u8 ,
                                message: format!("'{}': Error happened while submitting the domain catch requests: '{}'", Utc::now(), err),
                            });
                        }
                    }
                }
            }
        });
    }
}

