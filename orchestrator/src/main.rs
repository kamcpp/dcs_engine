mod config;
mod utils;
mod orchestrator;
mod catcher_manager;

use crate::config::Config;
use crate::orchestrator::{ORCHESTRATOR, Orchestrator};

use chrono::offset::TimeZone;

use std::time::Duration;
use std::sync::{Arc, Mutex};
use std::vec::Vec;
use std::clone::Clone;

use futures::join;

use tokio::time;
use tokio::task;
use tokio::net::TcpStream;
use tokio::io::{BufReader, AsyncWriteExt, AsyncBufReadExt};

use serde_json::error::Error;

use common::logger::Logger;
use common::domain::{DomainCatchRequest, Domain, ServicesResult, DomainMozSeoStat};

#[macro_use]
extern crate serde_derive;

type Result<T> = std::result::Result<T, Box<dyn std::error::Error + Send + Sync + 'static>>;

#[derive(Debug, Serialize)]
struct ScrapedDomain {
    domain: String,
    #[serde(rename = "catchTigerFinalBidPriceEur")]
    bid_price_eur: i32,
}

async fn read_all_domains(orchestrator: &Arc<Mutex<Orchestrator>>) -> Result<Vec<Domain>> {
    let uri;
    {
        let service = orchestrator.lock().unwrap();
        uri = format!("{}/api/engine/all_domains", service.config.services_base_uri());
    }
    utils::post(uri, "{}".to_string(), |body_str| {
        let data_err: std::result::Result<Vec<Domain>, Error> = serde_json::from_str(body_str);
        match data_err {
            Ok(domains) => {
                return Ok(domains);
            },
            Err(err) => return Err(format!("Error while desering all domains to json: {}", err).into()),
        }
    }).await
}

async fn read_domain_catch_requests(orchestrator: &Arc<Mutex<Orchestrator>>) -> Result<Vec<DomainCatchRequest>> {
    let uri;
    {
        let service = orchestrator.lock().unwrap();
        uri = format!("{}/api/engine/domain_catch_requests", service.config.services_base_uri());
    }
    utils::post(uri, "{}".to_string(), |body_str| {
        let data_err: std::result::Result<Vec<DomainCatchRequest>, Error> = serde_json::from_str(body_str);
        match data_err {
            Ok(domain_catch_requests) => {
                return Ok(domain_catch_requests);
            },
            Err(err) => return Err(format!("Error while desering domain catch requests to json: {}", err).into()),
        }
    }).await
}

async fn read_domains_without_catchdate(orchestrator: &Arc<Mutex<Orchestrator>>) -> Result<Vec<Domain>> {
    let uri;
    {
        let service = orchestrator.lock().unwrap();
        uri = format!("{}/api/engine/domains_without_catchdate", service.config.services_base_uri());
    }
    utils::post(uri, "{}".to_string(), |body_str| {
        let data_err: std::result::Result<Vec<Domain>, Error> = serde_json::from_str(body_str);
        match data_err {
            Ok(domains) => {
                return Ok(domains);
            },
            Err(err) => return Err(format!("Error while desering domains without catch date to json: {}", err).into()),
        }
    }).await
}

async fn read_domains_without_moz_seo_stat(orchestrator: &Arc<Mutex<Orchestrator>>) -> Result<Vec<Domain>> {
    let uri;
    {
        let service = orchestrator.lock().unwrap();
        uri = format!("{}/api/engine/domains_without_seo_data", service.config.services_base_uri());
    }
    utils::post(uri, "{}".to_string(), |body_str| {
        let data_err: std::result::Result<Vec<Domain>, Error> = serde_json::from_str(body_str);
        match data_err {
            Ok(domains) => {
                return Ok(domains);
            },
            Err(err) => return Err(format!("Error while desering domains without MOZ SEO data to json: {}", err).into()),
        }
    }).await
}

async fn send_scraped_domains(orchestrator: &Arc<Mutex<Orchestrator>>, scraped_domains: Vec<ScrapedDomain>) -> Result<()> {
    let uri;
    {
        let service = orchestrator.lock().unwrap();
        uri = format!("{}/api/engine/process_scraped_domains", service.config.services_base_uri());
    }
    utils::post(uri, serde_json::to_string(&scraped_domains).unwrap(), |body_str| {
        let data_err: std::result::Result<ServicesResult, Error> = serde_json::from_str(body_str);
        match data_err {
            Ok(result) => {
                if !result.result {
                    return Err(format!("Error while sending scraped domains: {:?}", result).into());
                }
                return Ok(());
            },
            Err(err) => return Err(format!("Error while desering services result to json: {}", err).into()),
        }
    }).await
}

async fn find_catchdate(domain: String) -> Result<String> {
    let to_return;
    let dac_server_addr = "dac.nic.uk:2043".to_string();
    let dac_tcp_stream = TcpStream::connect(dac_server_addr).await.unwrap();
    let mut dac_reader = BufReader::new(dac_tcp_stream);
    dac_reader.write_all(format!("{}\r\n", domain).as_bytes()).await.unwrap();
    let mut line = String::new();
    match dac_reader.read_line(&mut line).await {
        Ok(_) => {
            let tokens : Vec<&str> = line.split(",").collect();
            if tokens.len() >= 6 {
                match (tokens.len(), tokens[5]) {
                    (8, end_date) => {
                        let end_dt = chrono::DateTime::parse_from_str(format!("{} 00:00:00 +00:00" , end_date).as_str(), "%Y-%m-%d %H:%M:%S %:z").unwrap();
                        let catch_date = end_dt + chrono::Duration::days(92);
                        to_return = catch_date.format("%Y-%m-%d").to_string();
                    },
                    _ => {
                        println!("Wrong DAC result: '{}'", line);
                        to_return = "not-available".to_string();
                    }
                }
            } else {
                println!("Wrong length of DAC result: '{}'", line);
                to_return = "not-available".to_string();
            }
        },
        Err(err) => {
            println!("Error while finding catch date: {}", err);
            to_return = "error".to_string();
        }
    }
    println!("Found '{}' as the catch date for '{}'", to_return, domain);
    Ok(to_return)
}

async fn read_moz_seo_stat(domain: String) -> Result<DomainMozSeoStat> {
    println!("Reading MOZ data for {}", domain);
    let output =
        std::process::Command::new("python3")
            .current_dir("/src")
            .arg("moz.py")
            .arg(domain.clone())
            .output()
            .expect("Failed to execute domain scraper script");
    let moz_seo_stat_output = String::from_utf8(output.stdout).unwrap();
    println!("MOZ data: {}", moz_seo_stat_output);
    let tokens: Vec<&str> = moz_seo_stat_output.split("-").collect();
    let domain_authority: i16 = tokens[0].parse()?;
    let page_authority: i16 = tokens[1].parse()?;
    let moz_rank: f64 = tokens[2].parse()?;
    let links_in: i64 = tokens[3].parse()?;
    let mut last_token = tokens[4].to_string();
    last_token.pop();
    let external_equity_links: i64 = last_token.parse()?;
    Ok(DomainMozSeoStat {
        domain,
        domain_authority,
        page_authority,
        moz_rank,
        links_in,
        external_equity_links,
    })
}

fn main() -> std::result::Result<(), std::io::Error> {
    let orchestrator = Arc::new(Mutex::new(Orchestrator::new()));
    let mut rt = tokio::runtime::Runtime::new()?;
    let local = task::LocalSet::new();
    local.block_on(&mut rt, async move {
        {
            println!("Hello! Orchestrator is running now ...");
            let mut service = orchestrator.lock().unwrap();
            service.log_info(
                None,
                ORCHESTRATOR.to_string(),
                "Hello!".to_string(),
                "Orchestrator is running now ...".to_string(),
            );
        }
        let main_job;
        {
            let orchestrator = orchestrator.clone();
            main_job = task::spawn_local(async move {
                let mut interval = time::interval(Duration::from_secs(5));
                loop {
                    interval.tick().await;
                    match read_domain_catch_requests(&orchestrator).await {
                        Ok(domain_catch_requests) => {
                            catcher_manager::update_catchers(&orchestrator, domain_catch_requests).await;
                        },
                        Err(err) => {
                            let mut service = orchestrator.lock().unwrap();
                            service.log_error(
                                None,
                                ORCHESTRATOR.to_string(),
                                "Error while reading domain catch requests".to_string(),
                                format!("{}", err),
                            );
                        },
                    }
                }
            });
        }
        let catchdate_updater_job;
        {
            let orchestrator = orchestrator.clone();
            catchdate_updater_job = task::spawn_local(async move {
                let uri;
                {
                    let service = orchestrator.lock().unwrap();
                    uri = format!("{}/api/engine/update_domain_catchdate", service.config.services_base_uri());
                }
                let mut interval = time::interval(Duration::from_secs(5));
                loop {
                    interval.tick().await;
                    match read_domains_without_catchdate(&orchestrator).await {
                        Ok(domains) => {
                            for domain in domains {
                                match find_catchdate(domain.clone().domain).await {
                                    Ok(catchdate) => {
                                        let mut to_send = domain.clone();
                                        if catchdate != "error" && catchdate != "not-available" {
                                            let tokens: Vec<&str> = catchdate.split("-").collect();
                                            to_send.catch_date.year = tokens[0].parse::<u16>().unwrap();
                                            to_send.catch_date.month = tokens[1].parse::<u8>().unwrap();
                                            to_send.catch_date.day = tokens[2].parse::<u8>().unwrap();
                                        }
                                        utils::post(uri.clone(), serde_json::to_string(&to_send).unwrap(), |_| { Ok(()) }).await.ok();
                                    },
                                    Err(_) => {}
                                }
                            }
                        },
                        Err(err) => {
                            let mut service = orchestrator.lock().unwrap();
                            service.log_error(
                                None,
                                ORCHESTRATOR.to_string(),
                                "Error while reading domains without catch date".to_string(),
                                format!("{}", err),
                            );
                        },
                    }
                }
            });
        }
        let domain_purger_job;
        {
            let orchestrator = orchestrator.clone();
            domain_purger_job = task::spawn_local(async move {
                let uri;
                {
                    let service = orchestrator.lock().unwrap();
                    uri = format!("{}/api/engine/purge_domains", service.config.services_base_uri());
                }
                // TODO: Adjust the interval
                let mut interval = time::interval(Duration::from_secs(30 * 60));
                loop {
                    interval.tick().await;
                    match read_all_domains(&orchestrator).await {
                        Ok(domains) => {
                            let mut domain_ids = Vec::new();
                            for domain in domains {
                                if domain.catch_date.year == 2500 &&
                                   domain.catch_date.month == 1 &&
                                   domain.catch_date.day == 1 {
                                    continue;
                                }
                                let now = chrono::Utc::now();
                                let min_date = now - chrono::Duration::days(3);
                                let max_date = now + chrono::Duration::days(90);
                                let catch_date = chrono::Utc.ymd(domain.catch_date.year as i32, domain.catch_date.month as u32, domain.catch_date.day as u32).and_hms(0, 0, 0);
                                if min_date.signed_duration_since(catch_date).num_seconds() > 0 ||
                                   catch_date.signed_duration_since(max_date).num_seconds() > 0 {
                                    domain_ids.push(domain.id);
                                }
                            }
                            if domain_ids.len() > 0 {
                                utils::post(uri.clone(), serde_json::to_string(&domain_ids).unwrap(), |_| { Ok(()) }).await.ok();
                            }
                        },
                        Err(err) => {
                            let mut service = orchestrator.lock().unwrap();
                            service.log_error(
                                None,
                                ORCHESTRATOR.to_string(),
                                "Error while reading domains without catch date".to_string(),
                                format!("{}", err),
                            );
                        },
                    }
                }
            });
        }
        let schedule_job;
        {
            let orchestrator = orchestrator.clone();
            schedule_job = task::spawn_local(async move {
                let uri;
                {
                    let service = orchestrator.lock().unwrap();
                    uri = format!("{}/api/engine/schedule", service.config.services_base_uri());
                }
                // TODO: Adjust the interval
                let mut interval = time::interval(Duration::from_secs(6 * 60 * 60));
                interval.tick().await;
                loop {
                    interval.tick().await;
                    utils::post(uri.clone(), "{}".to_string(), |_| { Ok(()) }).await.ok();
                }
            });
        }
        let domain_scraper_job;
        {
            let orchestrator = orchestrator.clone();
            domain_scraper_job = task::spawn_local(async move {
                let mut interval = time::interval(Duration::from_secs(6 * 3600));
                interval.tick().await;
                loop {
                    interval.tick().await;
                    println!("Scraping expired domains ...");
                    let output =
                        std::process::Command::new("./scrape-domains.sh")
                            .current_dir("/src")
                            .output()
                            .expect("Failed to execute domain scraper script");
                    let domains_output = String::from_utf8(output.stdout).unwrap();
                    let mut scraped_domains = Vec::new();
                    let tokens: Vec<&str> = domains_output.split(",").collect();
                    let mut counter = 0;
                    loop {
                        let domain = tokens[counter].trim().to_lowercase().to_string();
                        match tokens[counter + 1].trim().parse::<i32>() {
                            Ok(bid_price_eur) => scraped_domains.push(ScrapedDomain { domain, bid_price_eur }),
                            Err(err) => println!("Error while parsing domain '{}': {}", domain, err),
                        }
                        counter += 2;
                        if counter >= tokens.len() {
                            break;
                        }
                    }
                    match send_scraped_domains(&orchestrator, scraped_domains).await {
                        Ok(_) => println!("Scraped domains are sent to services."),
                        Err(err) => println!("{}", err),
                    }
                }
            });
        }
        let domain_moz_seo_stat_reader_job;
        {
            let orchestrator = orchestrator.clone();
            domain_moz_seo_stat_reader_job = task::spawn_local(async move {
                let uri;
                {
                    let service = orchestrator.lock().unwrap();
                    uri = format!("{}/api/engine/update_domain_seo_data", service.config.services_base_uri());
                }
                let mut interval = time::interval(Duration::from_secs(5));
                loop {
                    interval.tick().await;
                    match read_domains_without_moz_seo_stat(&orchestrator).await {
                        Ok(domains) => {
                            for domain in domains {
                                match read_moz_seo_stat(domain.clone().domain).await {
                                    Ok(domain_moz_seo_stat) => {
                                        utils::post(uri.clone(), serde_json::to_string(&domain_moz_seo_stat).unwrap(), |_| { Ok(()) }).await.ok();
                                    },
                                    Err(err) => {
                                        let mut service = orchestrator.lock().unwrap();
                                        service.log_error(
                                            None,
                                            ORCHESTRATOR.to_string(),
                                            format!("Error while reading MOZ SEO data for domain '{}'", domain.clone().domain),
                                            format!("{}", err),
                                        );
                                    }
                                }
                            }
                        },
                        Err(err) => {
                            let mut service = orchestrator.lock().unwrap();
                            service.log_error(
                                None,
                                ORCHESTRATOR.to_string(),
                                "Error while reading domains without MOZ SEO data".to_string(),
                                format!("{}", err),
                            );
                        },
                    }
                }
            });
        }
        let (r1, r2, r3, r4, r5, r6) = join!(main_job, catchdate_updater_job, domain_purger_job, schedule_job, domain_scraper_job, domain_moz_seo_stat_reader_job);
        r1.expect("Couldn't start the main job!");
        r2.expect("Couldn't start the catch date updater job!");
        r3.expect("Couldn't start the domain purger job!");
        r4.expect("Couldn't start the schedule job!");
        r5.expect("Couldn't start the domain scraper job!");
        r6.expect("Couldn't start the domain MOZ SEO stats reader job!");
    });
    Ok(())
}
