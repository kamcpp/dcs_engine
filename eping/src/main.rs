use std::time::{Instant, Duration};
use std::error::Error;
use std::thread;

use clap::{App, Arg};

use tokio::net::TcpStream;
use tokio::io::{BufReader, AsyncWriteExt, AsyncBufReadExt};

use native_tls::TlsConnector;

macro_rules! hello_msg_fmt {() => ("
<?xml version=\"1.0\" encoding=\"UTF-8\"?>
  <epp xmlns=\"urn:ietf:params:xml:ns:epp-1.0\"
       xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
       xsi:schemaLocation=\"urn:ietf:params:xml:ns:epp-1.0
       epp-1.0.xsd\">
    <hello/>
  </epp>
")}

macro_rules! login_msg_fmt {() => ("
<?xml version=\"1.0\" encoding=\"UTF-8\"?>
  <epp xmlns=\"urn:ietf:params:xml:ns:epp-1.0\"
       xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
       xsi:schemaLocation=\"urn:ietf:params:xml:ns:epp-1.0 epp-1.0.xsd\">
    <command>
      <login>
        <clID>{}</clID>
        <pw>{}</pw>
        <options>
          <version>1.0</version>
          <lang>en</lang>
        </options>
        <svcs>
           <objURI>urn:ietf:params:xml:ns:domain-1.0</objURI>
           <objURI>urn:ietf:params:xml:ns:contact-1.0</objURI>
           <objURI>urn:ietf:params:xml:ns:host-1.0</objURI>
        </svcs>
      </login>
      <clTRID>ABC-12345</clTRID>
    </command>
  </epp>
")}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {

    ctrlc::set_handler( move || {
        println!();
        println!("Ctrl+C received ... Exiting ...");
        std::process::exit(0);
    }).expect("Error setting Ctrl+C handler");

    let matches = App::new("EPP Ping Program")
        .version("0.0.1")
        .author("Kamran Amini <kam.cpp@gmail.com>")
        .arg(Arg::with_name("host")
                    .short("h")
                    .long("host")
                    .takes_value(true)
                    .help("EPP host to use. Default: epp.nominet.org.uk"))
        .arg(Arg::with_name("port")
                    .short("p")
                    .long("port")
                    .takes_value(true)
                    .help("EPP port to use. Default: 700"))
        .arg(Arg::with_name("tag")
                    .short("t")
                    .long("tag")
                    .takes_value(true)
                    .required(true)
                    .help("Tag to use. [REQUIRED]"))
        .arg(Arg::with_name("password")
                    .short("P")
                    .long("password")
                    .takes_value(true)
                    .required(true)
                    .help("Password to use. [REQUIRED]"))
        .get_matches();

    let epp_host = matches.value_of("host").unwrap_or("epp.nominet.org.uk");
    let epp_port = match matches.value_of("port") {
        None => 700,
        Some(s) => {
            match s.parse::<u16>() {
                Ok(n) => n,
                Err(_) => {
                    println!("'{}' cannot be parsed as a network port!", s);
                    std::process::exit(1);
                }
            }
        }
    };
    let tag = matches.value_of("tag").unwrap();
    let password = matches.value_of("password").unwrap();

    println!("EPP: {}:{}", epp_host, epp_port);
    println!("Tag: {}", tag);
    println!("Password: {}", password);

    println!("---");

    let epp_addr = format!("{}:{}", epp_host, epp_port);

    let mut now = Instant::now();
    let socket = TcpStream::connect(epp_addr).await?;
    let tls_ctx = TlsConnector::builder().build().unwrap();
    let tls_ctx = tokio_native_tls::TlsConnector::from(tls_ctx);
    let socket = tls_ctx.connect(epp_host, socket).await?;
    let mut socket = BufReader::new(socket);

    let mut connect_diff = 0;
    loop {
        let mut line = String::new();
        socket.read_line(&mut line).await.ok();
        if connect_diff == 0 {
            connect_diff = now.elapsed().as_millis();
            println!("Connect: {} ms", connect_diff);
        }
        line = line.trim().to_string();
        if line.starts_with("</greeting") {
            break;
        }
    }

    now = Instant::now();
    socket.write_all(format!(login_msg_fmt!(), tag, password).as_bytes()).await?;
    let mut login_diff = 0;
    loop {
        let mut line = String::new();
        socket.read_line(&mut line).await.ok();
        if login_diff == 0 {
            login_diff = now.elapsed().as_millis();
            println!("Login: {} ms", login_diff);
        }
        line = line.trim().to_string();
        if line.starts_with("<result code=") {
            if !line.starts_with("<result code=\"1000\"") {
                println!("Login failed! The result line was: '{}'", line);
                std::process::exit(1);
            }
            break;
        }
    }

    println!("---");

    loop {
        let now = Instant::now();
        socket.write_all(hello_msg_fmt!().as_bytes()).await?;
        let mut hello_diff_printed = false;
        loop {
            let mut line = String::new();
            socket.read_line(&mut line).await.ok();
            if !hello_diff_printed {
                println!("Hello: {} ms", now.elapsed().as_millis());
                hello_diff_printed = true;
            }
            line = line.trim().to_string();
            if line.starts_with("</greeting") {
                break;
            }
        }
        thread::sleep(Duration::from_secs(1));
    }
}
