#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <netdb.h>
#include <assert.h>
#include <signal.h>
#include <math.h>
#include <pthread.h>

#include <sys/socket.h>
#include <sys/time.h>

#define TRUE 1
#define FALSE 0

typedef unsigned char bool_t;

typedef struct config_t {
    char* host;
    unsigned short port;
    unsigned int interval;
    char* command;
    bool_t is_async;
    bool_t print_response;
    unsigned short samples;
} config_t;

typedef struct writer_thread_data_t {
    int conn_sd;
    char* command;
    struct config_t* config;
} writer_thread_data_t;

long long current_millis() {
    struct timeval te;
    gettimeofday(&te, NULL);
    long long milliseconds = te.tv_sec * 1000LL + te.tv_usec / 1000;
    return milliseconds;
}

void* writer_thread_body(void* obj) {
    struct writer_thread_data_t* data = (struct writer_thread_data_t*)obj;
    size_t command_len = strlen(data->command);
    while(TRUE) {
        int result = write(data->conn_sd, data->command, command_len);
        if (result == -1) {
            fprintf(stderr, "Error while writing (async): %s\n", strerror(errno));
            exit(1);
        }
        // TODO(kam): Handle partial writes and remove this assert
        assert(result == command_len);
        usleep(data->config->interval * 1000);
    }
    return NULL;
}

int ping(struct config_t* config) {
    int conn_sd = socket(AF_INET, SOCK_STREAM, 0);
    if (conn_sd == -1) {
        fprintf(stderr, "Could not create socket: %s\n", strerror(errno));
        return 1;
    }
    struct hostent* host_entry;
    host_entry = gethostbyname(config->host);
    if (!host_entry) {
        fprintf(stderr, "Could not resolve the host '%s'\n", config->host);
        return 1;
    }
    struct sockaddr_in addr;
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr = *((struct in_addr*)host_entry->h_addr);;
    addr.sin_port = htons(config->port);
    int result = connect(conn_sd, (struct sockaddr*)&addr, sizeof(addr));
    if (result == -1) {
        close(conn_sd);
        fprintf(stderr, "Could no connect: %s\n", strerror(errno));
        return 1;
    }

    char command[256];
    snprintf(command, 256, "%s\r\n", config->command);
    size_t command_len = strlen(command);

    char reply[64];
    int return_code = 0;

    unsigned int counter = 0;
    double mean = 0.0, variance = 0.0, min = 0.0, max = 0.0;

    if (config->is_async) {
        struct writer_thread_data_t data;
        data.conn_sd = conn_sd;
        data.config = config;
        data.command = command;

        pthread_t writer_thread;
        pthread_create(&writer_thread, NULL, writer_thread_body, (void*)&data);

        while (TRUE) {
            long long now = current_millis();
            result = read(conn_sd, reply, 64);
            if (result == -1) {
                fprintf(stderr, "Error while reading (async): %s\n", strerror(errno));
                return_code = 1;
                break;
            }
            char* end = strstr(reply, "\r");
            if (end) {
                *end = '\0';
                long long diff = current_millis() - now;
                counter++;
                if (counter == 1 || diff < min) {
                    min = diff;
                }
                if (counter == 1 || diff > max) {
                    max = diff;
                }
                double new_mean = (mean * (counter - 1) + diff) / (double)counter;
                variance = ((counter - 1) * variance + (counter - 1) * (mean - new_mean) * (mean - new_mean) + (diff - new_mean) * (diff - new_mean)) / (double)counter;
                mean = new_mean;
                if (config->print_response) {
                    printf("%lld ms: %s\n", diff, reply);
                    fflush(stdout);
                }
                if (counter == config->samples) {
                    printf("[C] Wait time between DAC responses > \t MEAN: %8.2f ms \t STD-DEV: %8.3f ms \t MIN: %8.2f ms \t MAX: %8.2f ms\n", mean, sqrt(variance), min, max);
                    fflush(stdout);
                    mean = variance = min = max = 0.0;
                    counter = 0;
                }
            } else {
                printf("[C] WRANING: partial reply found! (async)\n");
                // TODO(kam): Handle multi-part replies
            }
        }
    } else {
        while (TRUE) {
            long long now = current_millis();
            result = write(conn_sd, command, command_len);
            if (result == -1) {
                fprintf(stderr, "Error while writing (sync): %s\n", strerror(errno));
                return_code = 1;
                break;
            }
            // TODO(kam): Handle partial writes and remove this assert
            assert(result == command_len);
            result = read(conn_sd, reply, 64);
            if (result == -1) {
                fprintf(stderr, "Error while reading (sync): %s\n", strerror(errno));
                return_code = 1;
                break;
            }
            char* end = strstr(reply, "\r");
            if (end) {
                *end = '\0';
                long long diff = current_millis() - now;
                counter++;
                if (counter == 1 || diff < min) {
                    min = diff;
                }
                if (counter == 1 || diff > max) {
                    max = diff;
                }
                double new_mean = (mean * (counter - 1) + diff) / (double)counter;
                variance = ((counter - 1) * variance + (counter - 1) * (mean - new_mean) * (mean - new_mean) + (diff - new_mean) * (diff - new_mean)) / (double)counter;
                mean = new_mean;
                if (config->print_response) {
                    printf("[C] %lld ms: %s\n", diff, reply);
                    fflush(stdout);
                }
                if (counter == config->samples) {
                    printf("[C] Wait time for DAC response > \t MEAN: %8.2f ms \t STD-DEV: %8.3f ms \t MIN: %8.2f ms \t MAX: %8.2f ms\n", mean, sqrt(variance), min, max);
                    fflush(stdout);
                    mean = variance = min = max = 0.0;
                    counter = 0;
                }
            } else {
                printf("[C] WRANING: partial reply found! (sync)\n");
                // TODO(kam): Handle multi-part replies
            }
            usleep(config->interval * 1000);
        }
    }
    return return_code;
}

void sigint_handler(int dummy) {
    printf("SIGINT received ... existing ...\n");
    exit(1);
}

int main(int argc, char** argv) {

    signal(SIGINT, sigint_handler);

    struct config_t config;
    config.host = "dac.nic.uk";
    config.port = 3043;
    config.interval = 63;
    config.command = "bbc.co.uk";
    config.is_async = TRUE;
    config.print_response = FALSE;
    config.samples = 100;

    int result = ping(&config);
    return result;
}
