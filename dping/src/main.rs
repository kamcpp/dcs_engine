use std::time::Duration;
use std::result::Result;
use std::thread;

use clap::{App, Arg};
use chrono::Utc;
use tokio::task;
use tokio::time;
use tokio::net::TcpStream;
use tokio::io::{BufReader, AsyncWriteExt, AsyncBufReadExt};
use statrs::statistics::Statistics;

#[derive(Clone, Debug)]
struct Config {
    host: String,
    port: u16,
    interval: u64,
    command: String,
    is_async: bool,
    print_response: bool,
    samples: u16,
}

async fn ping(config: Config) -> Result<(), std::io::Error> {
    let dac_addr = format!("{}:{}", config.host, config.port);
    let dac_tcp_stream = TcpStream::connect(dac_addr).await.unwrap();
    let mut counter : u64 = 1;
    let mut marks = Vec::new();
    let mut mark = Utc::now();
    if !config.is_async {
        let mut dac_reader = BufReader::new(dac_tcp_stream);
        loop {
            let mut line = String::new();
            mark = Utc::now();
            dac_reader.write(format!("{}\r\n", config.command).as_bytes()).await?;
            dac_reader.read_line(&mut line).await?;
            line.pop(); line.pop();
            let diff = f64::from(Utc::now().signed_duration_since(mark).num_milliseconds() as i32);
            marks.push(diff);
            if counter % (config.samples as u64) == 0 {
                {
                    let min = marks.clone().min();
                    let max = marks.clone().max();
                    let mean = marks.clone().mean();
                    let std_dev = marks.clone().population_std_dev();
                    println!("[Rust] Wait time for DAC response > \t MEAN: {:8.2} ms \t STD-DEV: {:8.3} ms \t MIN: {:8.2} ms \t MAX: {:8.2} ms", mean, std_dev, min, max);
                    marks.clear();
                }
            }
            if config.print_response {
                println!("{}: {} ms", line, diff as u32);
            }
            counter += 1;
            thread::sleep(Duration::from_millis(config.interval));
        }
    } else {
        let (dac_tcp_reader, mut dac_tcp_writer) = dac_tcp_stream.into_split();
        let mut dac_reader = BufReader::new(dac_tcp_reader);
        {
            let config = config.clone();
            task::spawn_local(async move {
                let mut interval = time::interval(Duration::from_millis((config.interval).into()));
                loop {
                    interval.tick().await;
                    match dac_tcp_writer.write_all(format!("{}\r\n", config.command).as_bytes()).await {
                        Ok(_) => {},
                        Err(err) => {
                            println!("ERROR: {}", err);
                            std::process::exit(1);
                        }
                    }
                }
            });
        }
        loop {
            let mut line = String::new();
            match dac_reader.read_line(&mut line).await {
                Ok(_) => {
                    let diff = f64::from(Utc::now().signed_duration_since(mark).num_milliseconds() as i32);
                    marks.push(diff);
                    mark = Utc::now();
                    if counter % (config.samples as u64) == 0 {
                        {
                            let min = marks.clone().min();
                            let max = marks.clone().max();
                            let mean = marks.clone().mean();
                            let std_dev = marks.clone().population_std_dev();
                            println!("[Rust] Wait time between DAC responses > MEAN: {:8.2} ms, STD-DEV: {:8.3} ms, MIN: {:8.2} ms, MAX: {:8.2} ms", mean, std_dev, min, max);
                            marks.clear();
                        }
                    }
                    if config.print_response {
                        line = line.trim().to_string();
                        println!("{}: {} ms", line, diff as u32);
                    }
                    counter += 1;
                }
                Err(err) => {
                    println!("ERROR: {}", err);
                    std::process::exit(1);
                },
            }
        }
    }
}

fn main() -> Result<(), std::io::Error> {
    let config;
    {
        ctrlc::set_handler( move || {
            println!();
            println!("Ctrl+C received ... Exiting ...");
            std::process::exit(0);
        }).expect("Error setting Ctrl+C handler");

        let matches = App::new("DAC Ping Program")
            .version("0.0.1")
            .author("Kamran Amini <kam.cpp@gmail.com>")
            .arg(Arg::with_name("host")
                        .short("h")
                        .long("host")
                        .takes_value(true)
                        .help("DAC host to use. Default: dac.nic.uk"))
            .arg(Arg::with_name("port")
                        .short("p")
                        .long("port")
                        .takes_value(true)
                        .help("DAC port to use. Default: 3043"))
            .arg(Arg::with_name("command")
                        .short("c")
                        .long("command")
                        .takes_value(true)
                        .help("DAC command to send. Default: #usage"))
            .arg(Arg::with_name("interval")
                        .short("i")
                        .long("interval")
                        .takes_value(true)
                        .help("Interval in milliseconds between DAC requests. Default: 1000"))
            .arg(Arg::with_name("samples")
                        .long("samples")
                        .takes_value(true)
                        .help("Number of samples required for calculating stats. Default: 100"))
            .arg(Arg::with_name("print-response")
                        .long("print-response")
                        .takes_value(false)
                        .help("Print DAC response. Default: false"))
            .arg(Arg::with_name("sync")
                        .short("s")
                        .long("sync")
                        .takes_value(false)
                        .help("Send sync DAC requests. Cannot be used with -a or --async. By default async is enabled."))
            .arg(Arg::with_name("async")
                        .short("a")
                        .long("async")
                        .takes_value(false)
                        .help("Send async DAC requests. Cannot be used with -s or --sync. This is enabled by default."))
            .get_matches();

        if matches.is_present("sync") && matches.is_present("async") {
            println!("You cannot specify both -a (--aysnc) and -s (--sync) at the same time.");
            std::process::exit(1);
        }

        let host = matches.value_of("host").unwrap_or("dac.nic.uk");
        let port = match matches.value_of("port") {
            None => 3043,
            Some(s) => {
                match s.parse::<u16>() {
                    Ok(n) => n,
                    Err(_) => {
                        println!("'{}' cannot be parsed as a network port!", s);
                        std::process::exit(1);
                    }
                }
            }
        };
        let interval = match matches.value_of("interval") {
            None => 1000,
            Some(s) => {
                match s.parse::<u64>() {
                    Ok(n) => n,
                    Err(_) => {
                        println!("'{}' cannot be parsed as a millisecond interval!", s);
                        std::process::exit(1);
                    }
                }
            }
        };
        let samples = match matches.value_of("samples") {
            None => 100,
            Some(s) => {
                match s.parse::<u16>() {
                    Ok(n) => n,
                    Err(_) => {
                        println!("'{}' cannot be parsed as number of samples!", s);
                        std::process::exit(1);
                    }
                }
            }
        };
        let command = matches.value_of("command").unwrap_or("#usage");
        let mut is_async = true;
        if matches.is_present("sync") {
            is_async = false;
        }
        let mut print_response = false;
        if matches.is_present("print-response") {
            print_response = true;
        }

        config = Config {
            host: host.to_string(),
            port,
            interval,
            command: command.to_string(),
            is_async,
            print_response,
            samples,
        };
    }

    println!("{:?}", config);
    println!("---");

    let mut rt = tokio::runtime::Runtime::new()?;
    let local = task::LocalSet::new();
    local.block_on(&mut rt, ping(config))
}
