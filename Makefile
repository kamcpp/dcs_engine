.PHONY: all-dev
all-dev: orchestrator-dev catcher-dev dping-dev eping-dev

.PHONY: push
push: push-orchestrator push-catcher push-dping push-eping

.PHONY: push-orchestrator
push-orchestrator:
	docker push dcsys/orchestrator:latest

.PHONY: push-catcher
push-catcher:
	docker push dcsys/catcher:latest

.PHONY: push-dping
push-dping:
	docker push dcsys/dping:latest

.PHONY: push-c-dping
push-c-dping:
	docker push dcsys/c-dping:latest

.PHONY: push-eping
push-eping:
	docker push dcsys/eping:latest

.PHONY: rust
rust:
	docker build --rm -f Dockerfile.rust -t dcsys/rust:latest .

.PHONY: orchestrator-dev
orchestrator-dev: rust
	docker build --rm -f Dockerfile.orchestrator-dev -t dcsys/orchestrator:latest .

.PHONY: catcher-dev
catcher-dev: rust
	docker build --rm -f Dockerfile.catcher-dev -t dcsys/catcher:latest .

.PHONY: dping-dev
dping-dev: rust
	docker build --rm -f Dockerfile.dping-dev -t dcsys/dping:latest .

.PHONY: c-dping-dev
c-dping-dev:
	docker build --rm -f Dockerfile.c-dping-dev -t dcsys/c-dping:latest .

.PHONY: eping-dev
eping-dev: rust
	docker build --rm -f Dockerfile.eping-dev -t dcsys/eping:latest .

