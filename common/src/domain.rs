use std::hash::Hash;
use std::cmp::Ordering;

use derivative::Derivative;

#[derive(Debug, Clone)]
pub enum CatchSettingStatus {
    Unknown = 0,
    Queued = 1,
    PickedUp = 2,
    Disabled = 3,
    Catching = 4,
    DomainLost = 5,
    DomainRegistered = 6,
    DomainNeverDropped = 7,
    Aborted = 8,
    Error = 9,
    EndTimePassed = 10,
    Blocked = 11,
}

#[derive(Debug, Clone)]
pub enum CatcherStatus {
    Unknown = 0,
    HealthyFree = 1,
    HealthyCatching = 2,
    NotHealthy = 3,
}

#[derive(Deserialize, Serialize, Debug, Clone, Eq, Hash, PartialEq)]
pub struct Time {
    pub hour: i8,
    pub minute: i8,
}

impl Ord for Time {
    fn cmp(&self, other: &Self) -> Ordering {
        let result = self.hour.cmp(&other.hour);
        if result != Ordering::Equal {
            return result;
        }
        return self.minute.cmp(&other.minute);
    }
}

impl PartialOrd for Time {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

#[derive(Deserialize, Serialize, Debug, Clone, Eq, Hash, PartialEq)]
pub struct Date {
    pub year: u16,
    pub month: u8,
    pub day: u8,
}

impl Ord for Date {
    fn cmp(&self, other: &Self) -> Ordering {
        let result = self.year.cmp(&other.year);
        if result != Ordering::Equal {
            return result;
        }
        let result = self.month.cmp(&other.month);
        if result != Ordering::Equal {
            return result;
        }
        return self.day.cmp(&other.day);
    }
}

impl PartialOrd for Date {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

#[derive(Derivative, Deserialize, Serialize)]
#[derivative(Debug, Clone, Eq, Hash, PartialEq)]
pub struct Domain {
    #[derivative(PartialEq="ignore")]
    #[derivative(Hash="ignore")]
    pub id: u64,
    pub domain: String,
    #[serde(rename = "catchDate")]
    pub catch_date: Date,
    pub priority: u8,
}

#[derive(Derivative, Deserialize, Serialize)]
#[derivative(Debug, Clone, Eq, Hash, PartialEq)]
pub struct Catcher {
    #[derivative(PartialEq="ignore")]
    #[derivative(Hash="ignore")]
    pub id: u64,
    pub name: String,
    pub host: String,
    pub port: u16,
}

#[derive(Derivative, Deserialize, Serialize)]
#[derivative(Debug, Clone, Eq, Hash, PartialEq)]
pub struct EppTag {
    #[derivative(PartialEq="ignore")]
    #[derivative(Hash="ignore")]
    pub id: u64,
    pub name: String,
    pub host: String,
    pub port: u16,
    pub tag: String,
    pub password: String,
    #[serde(rename = "accountId")]
    pub account_id: String,
}

#[derive(Derivative, Deserialize, Serialize)]
#[derivative(Debug, Clone, Eq, Hash, PartialEq)]
pub struct CatchSetting {
    #[derivative(PartialEq="ignore")]
    #[derivative(Hash="ignore")]
    pub id: u64,
    #[serde(rename = "startTime")]
    pub start_time: Time,
    #[serde(rename = "endTime")]
    pub end_time: Time,
    #[serde(rename = "dacHost")]
    pub dac_host: String,
    #[serde(rename = "dacPort")]
    pub dac_port: u16,
    #[serde(rename = "dacInterval")]
    pub dac_interval: u32,
    #[serde(rename = "registerPeriodYears")]
    pub register_period_years: u8,
    #[serde(rename = "statusCode")]
    pub status_code: u8,
}

#[derive(Deserialize, Serialize, Debug, Clone, Eq, Hash, PartialEq)]
pub struct DomainCatchRequest {
    pub domain: Domain,
    pub catcher: Catcher,
    #[serde(rename = "eppTag")]
    pub epp_tag: EppTag,
    #[serde(rename = "catchSetting")]
    pub catch_setting: CatchSetting,
}

#[derive(Deserialize, Debug)]
pub struct ServicesResult {
    pub result: bool,
    pub message: String,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct StatusEntry {
    pub entity: String,
    pub id: u64,
    #[serde(rename = "statusCode")]
    pub code: u8,
    #[serde(rename = "statusMessage")]
    pub message: String,
}

#[derive(Serialize, Debug, Clone)]
pub struct DomainMozSeoStat {
    pub domain: String,
    #[serde(rename = "domainAuthority")]
    pub domain_authority: i16,
    #[serde(rename = "pageAuthority")]
    pub page_authority: i16,
    #[serde(rename = "mozRank")]
    pub moz_rank: f64,
    #[serde(rename = "linksIn")]
    pub links_in: i64,
    #[serde(rename = "externalEquityLinks")]
    pub external_equity_links: i64,
}

