pub trait Logger {
    fn log_trace(&mut self, catch_setting_id: Option<u64>, source: String, title: String, msg: String);
    fn log_debug(&mut self, catch_setting_id: Option<u64>, source: String, title: String, msg: String);
    fn log_info(&mut self, catch_setting_id: Option<u64>, source: String, title: String, msg: String);
    fn log_warn(&mut self, catch_setting_id: Option<u64>, source: String, title: String, msg: String);
    fn log_error(&mut self, catch_setting_id: Option<u64>, source: String, title: String, msg: String);
    fn log_fatal(&mut self, catch_setting_id: Option<u64>, source: String, title: String, msg: String);
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct LogEntry {
    #[serde(rename="catchSettingId")]
    pub catch_setting_id: Option<u64>,
    pub time: i64,
    pub severity: i8,
    pub source: String,
    pub title: String,
    pub msg: String,
}

#[derive(Debug)]
pub enum LogSeverity {
    Trace = 1,
    Debug = 2,
    Info = 3,
    Warning = 4,
    Error = 5,
    Fatal = 6,
}
